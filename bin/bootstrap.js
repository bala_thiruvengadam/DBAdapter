'use strict';
var mongoose = require('mongoose'),
  initService = require('../app/ui/services/bootstrap.server.services'),
  logger = require('../app/core/utils/logger.server.util');

mongoose.Promise = require('bluebird');
var schedule = require('node-schedule');
///////////////////////////////////////////////////////////////////////////
//  Start Mongoose and connect to Mongodb
///////////////////////////////////////////////////////////////////////////
module.exports = function() {
    //mongoose.Promise = global.Promise;
    mongoose.connect(
      'mongodb://kryptos:kryptos@ds159507.mlab.com:59507/dbadapter',
      function(err) {
        if (err) {
          logger.error(__filename,
            " - Critical error: Couldn't start mongodb instance:"
          );
          logger.error(err);
          return;
        }
        // Initialize the tenant configurations and db connections
        initService.initializeTenants(function(err, results) {
          logger.info(__filename, " - Initialization completed !!!");
          ////////////////////////////////////////////////////////////////////////////////
          // Start the schedulre job for handling expired tickets
          ////////////////////////////////////////////////////////////////////////////////
          var ticketExpJob = schedule.scheduleJob('*/2 * * * *', function() {
            logger.info(__filename, " - Starting scheduler at " + new Date());
            initService.clearExpiredTickets(function() {
              logger.info(__filename, " - Excution of job over at " + new Date());
              logger.info(JSON.stringify(ticketExpJob));
            });
          });
        });
      });
    var db = mongoose.connection;
    db.on('error', function callback(err) {
      logger.error(__filename, " - Database connection failed. Error: " + err);
    });
    db.once('open', function callback() {
      logger.info(__filename, " - Database connection successful.");
    });
  }
  //////////////////////////////////////////////////////////////////////////
