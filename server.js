'use strict';
////////////////////////////////////////////////////////////////////////////////
//  Define and initialize the global environment variables
////////////////////////////////////////////////////////////////////////////////
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
global.serverURL = "http://localhost:3000/"
global.kryptosURL = "https://kryptos.kryptosmobile.com/"
    ////////////////////////////////////////////////////////////////////////////////
    //  Bootstrap section
    ////////////////////////////////////////////////////////////////////////////////
    //var express = require('./config/express');
var mongoose = require('mongoose');

///////////////////////////////////////////////////////////////////////////
//  Start Mongoose and connect to Mongodb
///////////////////////////////////////////////////////////////////////////
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://kryptos:kryptos@ds159507.mlab.com:59507/dbadapter',
    function(err) {
        if (err) {
            console.log("Critical error: Couldn't start mongodb instance:");
            console.log(err);
            return;
        }
        // Bootstrap the tenant configurations and db connections

    });
var db = mongoose.connection;
db.on('error', function callback(err) {
    console.log("Database connection failed. Error: " + err);
});
db.once('open', function callback() {
    console.log("Database connection successful.");
});
//////////////////////////////////////////////////////////////////////////

var oraconnect = require('./app/dao/dbconnect.server.dao');

var orapool = oraconnect();

////////////////////////////////////////////////////////////////////////////////
//  Start the application
////////////////////////////////////////////////////////////////////////////////
var app = express();
app.listen(3000);
module.exports = app;

console.log("Server running at http://localhost:3000/");

process.on('uncaughtException', function(err) {
    console.log(err);
});
