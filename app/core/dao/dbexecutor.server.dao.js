'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Module function executes SQL section meta data from service definitions
//	and returns the results in Object format
////////////////////////////////////////////////////////////////////////////////
var oracledb = require('oracledb'),
	async = require('async'),
	_ = require('lodash'),
	logger = require('../utils/logger.server.util');

var numRows = 100; // number of rows to return from each call to getRows()



function fetchResultSetRows(resultSet, numRows, key, datarows, rsCallBack) {
	resultSet.getRows(
		numRows,
		function(err, rows) {
			if (err) {
				key = "error";
				datarows = err.message;
				logger.error(__filename, "- Error fetching resultset rows: " + err)
				doClose(resultSet, key, datarows, function(err, key, data, status) {
					return rsCallBack(data, key, null);
				});
			} else if (rows.length === 0) {
				logger.debug(__filename, "No more records to fetch");
				// Close result set and return data
				doClose(resultSet, key, datarows, function(err, key, data, status) {
					rsCallBack(null, key, data);
					return;
				});
			} else if (rows.length > 0) {
				datarows.push(rows);
				fetchResultSetRows(resultSet, numRows, key, datarows, rsCallBack);
			}
		});
}


function doRelease(connection) {
	connection.release(
		function(err) {
			if (err) {
				logger.error(__filename, "- Error in doRelease method while releasing the connection", err.message);
			}
		});
}

function doClose(resultSet, key, data, rsCloseCallback) {
	resultSet.close(
		function(err) {
			if (err) {
				logger.error(__filename, "Error in doClose method:", err.message);
				rsCloseCallback(err, key, data, err.message);
				return;
			}
			logger.debug(__filename, "- Resultset closed");
			rsCloseCallback(null, key, data, "Result set closed");
			return;
		});
}

function execSQL(connection, section, sqlCallBack) {
	try {
		connection.execute(
			section.query,
			section.parameterObject, {
				maxRows: 300,
				outFormat: oracledb.OBJECT,
				//extendedMetaData: true,
				autoCommit: true
			},
			function(err, result) {
				var sectionName = section.name;
				if (err) {
					//console.error(err.message);
					sqlCallBack(null, {
						"error": err.message
					});
					return;
				}
				var resultMap = {};
				//////////////////////////////////////////////////////////
				resultMap[sectionName] = result.rows
				sqlCallBack(null, resultMap);
				return;
			});
	} catch (err) {
		return sqlCallBack(null, {
			"error": "Couldn't execute the SQL services."
		});
	}
}

function execProcedure(connection, section, procCallBack) {
	connection.execute(
		section.query,
		section.parameterObject, {
			outFormat: oracledb.OBJECT,
			extendedMetaData: true,
			autoCommit: true
		},
		function(err, result) {
			var sectionName = section.name;
			if (err) {
				//console.error(err.message);
				procCallBack(null, {
					"error": err.message
				});
				return;
			}
			var outParams = [],
				outCount = 0;
			for (outCount = 0; outCount < section.parameters.length; outCount++) {
				if (section.parameters[outCount].category == "OUT") {
					outParams.push(section.parameters[outCount]);
				}
			}
			logger.debug(__filename, "Out params :", outParams);
			async.map(outParams,
				function(param, paramCallback) {
					var resultMap = {};
					if (param.category == "OUT") {
						switch (param.type) {
							case "CURSOR":
								logger.debug(__filename, "- OUT Parameter ", param.key, " is of CURSOR type.");
								var dataMap = [];
								fetchResultSetRows(this.result.outBinds[param.key], numRows, param.key,
									dataMap,
									function(err, key, data) {
										resultMap[key] = _.chunk(data[0], 1);
										paramCallback(err, resultMap);
										return;
									});
								break;
							case "NUMBER":
								logger.debug(__filename, "- OUT Parameter ", param.key, " is of NUMBER type.");
								resultMap[param.key] = this.result.outBinds[param.key];
								paramCallback(null, resultMap);
								break;
							case "DATE":
								logger.debug(__filename, "- OUT Parameter ", param.key, " is of DATE type.");
								resultMap[param.key] = this.result.outBinds[param.key];
								paramCallback(null, resultMap);
								break;
							case "CLOB":
								logger.debug(__filename, "- OUT Parameter ", param.key, " is of CLOB type.");
								var clob = "";
								var lob = this.result.outBinds[param.key];

								if (lob === null) {
									resultMap[param.key] = [];
									paramCallback(null, resultMap);
									return;
								}
								lob.setEncoding('utf8');
								lob.on('data',
									function(chunk) {
										clob += chunk;
										return;
									});
								lob.on('end',
									function() {
										return;
									});
								lob.on('close',
									function() {
										if (clob) {
											/////////////////////////////////////////////
											// format CLOB result
											var stringKey = clob.substring(0, clob.indexOf("_##_"));
											var stringVal;
											if (stringKey) {
												stringVal = clob.substring(clob.indexOf("_##_") + 4);
												var keyValObj = {};
												keyValObj[stringKey] = JSON.parse(stringVal);
												resultMap[param.key] = [];
												resultMap[param.key].push(keyValObj);
											} else {
												resultMap[param.key] = clob;
											}
											////////////////////////////////////////////
											//console.log(data);
											paramCallback(null, resultMap);
											return;
										}
									});
								lob.on('error',
									function(err) {
										if (err) {
											logger.error(__filename, "- In OUT parameter clob error", err);
											clob = {
												" error": err.message
											};
											return;
										}
									});
								break;
							case "VARCHAR":
							case "VARCHAR2":
							default:
								logger.debug(__filename, "- OUT Parameter ", param.key, " is of STRING type.");
								var strResult = this.result.outBinds[param.key];
								/////////////////////////////////////////////
								// format Text result
								var stringKey = strResult.substring(0, strResult.indexOf("_##_"));
								var stringVal;
								if (stringKey) {
									stringVal = strResult.substring(strResult.indexOf("_##_") + 4);
									var keyValObj = {};
									keyValObj[stringKey] = JSON.parse(stringVal);
									resultMap[param.key] = [];
									resultMap[param.key].push(keyValObj);
								} else {
									resultMap[param.key] = strResult;
								}
								paramCallback(null, resultMap);
								break;
						}
					}
				}.bind({
					"result": result
				}),
				function(err, outRes) {
					//console.log("Outres map");
					//console.log(outRes.length);
					var formattedResObj = {},
						formatCounter = 0;
					var resKeys;

					for (formatCounter = 0; formatCounter < outRes.length; formatCounter++) {
						resKeys = Object.keys(outRes[formatCounter]);
						//console.log(resKeys[0]);
						var resKeyText = resKeys[0];
						formattedResObj[resKeyText] = outRes[formatCounter][resKeyText];
						//console.log(outRes[formatCounter][resKeyText]);
					}
					procCallBack(null, formattedResObj);
					return;
				});
		});
}

///////////////////////////////////////////////////////////////////////////
//	Execute sections
///////////////////////////////////////////////////////////////////////////
var executeSections = function(section, callBack) {
	//console.log(JSON.stringify(section));
	var sectionName = section.name;
	logger.info(__filename, "- Arguments for section", sectionName, "are:");
	logger.info({
		queryName: sectionName,
		invocationType: section.invocationtype,
		paramMap: section.parameterObject
	});
	if (section.invocationtype == "SQL") {
		execSQL(this.connection, section, function(err, results) {
			logger.info(__filename, "- Returning result:");
			logger.info(results);
			callBack(err, results);
			return;
		});
	} else if (section.invocationtype == "PROC") {
		execProcedure(this.connection, section, function(err, results) {
			logger.info(__filename, "- Returning result:");
			logger.info(results);
			callBack(err, results);
		});
	}
}

function getConnection(config, sections, callBack) {
	var poolName = config.appName,
		dbConfigs = config.dataSourceConfig,
		oraPool;
	logger.debug(__filename, "- Getting DB connection from pool ", poolName);
	oraPool = require('./dbconnect.server.dao')(poolName, dbConfigs, function(err, connection) {
		if (err) {
			logger.error(__filename, "- ERROR:" + err);
			return callBack(err);
		}
		return callBack(null, sections, connection);
	});

}


module.exports = function(config, sections, callback) {
	// Get a connection from pool and execute sections.
	getConnection(config, sections, function(err, sections, connection) {
		if (err) {
			callback(err, null);
			return;
		}
		logger.debug(__filename, "- Connection established from pool ", config.appName);
		var holdResults = {};
		async.map(sections,
			executeSections.bind({
				"connection": connection
			}),
			function(err, results) {
				if (err) {
					logger.error(__filename, "Error:", err);
				}
				logger.debug(__filename, "- Releasing connection after all services executed:");
				doRelease(connection);
				var i = 0,
					j = 0;
				var formattedResObj = {};
				logger.debug(__filename, "- Module result:");
				_.forEach(results, function(result) {
					var keys = Object.keys(result);
					_.forEach(keys, function(vkey) {
						var key = vkey,
							value = result[key];
						//console.log("Key= " + key + " - value=" + value);
						formattedResObj[key] = value;
					});
				});
				logger.debug(formattedResObj);
				callback(null, formattedResObj);
			});
	});
}
