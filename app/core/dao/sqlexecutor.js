var oracledb = require('oracledb');
	//bannerpool = require('../db/dbconnect');

//var pool = bannerpool();
function doRelease(connection) {
	console.log("closing connection");
	connection.release(
		function(err) {
			if (err) {
				console.error(err);
			}
		});
}

module.exports = function(sql, paramObj, callback) {
	oracledb.getConnection('bannerpool', function(err, connection) {
		if (err) {
			console.log("error in getting connection");
			console.error(err.message);
			callback(err, null);
			return;
		}
		connection.execute(
			sql, paramObj, {
				outFormat: oracledb.OBJECT,
				extendedMetaData: true
			},
			function(err, result) {
				if (err) {
					console.error(err.message);
					doRelease(connection);
					callback(err,null);
					return;
				}
				//console.log("Header meta info:");
				//console.log(result.metaData);
				//console.log("Result data:");
				var data = result.rows;
				//console.log(JSON.stringify(data));
				doRelease(connection);
				callback(null,data);
				return;
			});
	});
}