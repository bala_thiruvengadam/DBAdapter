'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Create Banner database pools
////////////////////////////////////////////////////////////////////////////////
var crypto = require('../utils/crypto.server.util');
var oracledb = require('oracledb');
var logger = require('../utils/logger.server.util');

module.exports = function(poolName, dbConfigs, callBack) {
	var bannerpool, err = {};
	if (!poolName) {
		logger.error(__filename, '- Error: Poolname not mentioned, skip creating pool.');
		err = {
			"statuscode": 500,
			"error": "Poolname not mentioned, skip creating pool."
		}
		return callBack(err);
	}
	logger.debug(__filename, "- Searching for database connection pool " + poolName + ":");
	try {
		bannerpool = oracledb.getPool(poolName);
	} catch (ex) {
		console.log(ex);
		bannerpool = "";
	}
	if (bannerpool) {
		logger.debug(__filename, "- Database pool " + poolName +
			" already exists. Getting connection from pool...");
		bannerpool.getConnection(function(err, connection) {
			if (err) {
				logger.error(__filename, "- Error getting connection from pool");
				logger.error(err);
				return callBack(err);
			}
			return callBack(null, connection);
		});
	} else {
		logger.warn(__filename,
			"- Database connection pool doesn't exist. Creating new connection pool :" + poolName
		);
		oracledb.createPool({
			poolAlias: poolName,
			user: dbConfigs.username,
			password: crypto.decrypt(dbConfigs.password),
			connectString: dbConfigs.connectString,
			//connectString: "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.56.102)(PORT=1521))(CONNECT_DATA=(SERVER=DEDICATED)(SID=BAN83)))",
			//connectString: "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=localhost)(PORT=8888))(CONNECT_DATA=(SERVER=DEDICATED)(SID=BAN9)))",
			poolMax: dbConfigs.poolMax ? dbConfigs.poolMax : 2,
			poolMin: dbConfigs.poolMin ? dbConfigs.poolMin : 1,
			poolIncrement: dbConfigs.poolIncrement ? dbConfigs.poolIncrement : 1,
			poolTimeout: dbConfigs.poolTimeout ? dbConfigs.poolTimeout : 60,
			poolPingInterval: dbConfigs.poolPingInterval ? dbConfigs.poolPingInterval : 60,
			queueRequests: dbConfigs.queueRequests ? dbConfigs.queueRequests : true,
			queueTimeout: dbConfigs.queueTimeout ? dbConfigs.queueTimeout : 6000


		}, function(err, pool) {
			if (err) {
				logger.error(__filename, "- Error creating new connection pool:", poolName);
				logger.error(err);
				return callBack(err);
			}
			//console.log("Created and got the 'bannerpool' object reference.");
			bannerpool = pool;
			bannerpool.getConnection(function(err, connection) {
				if (err) {
					logger.error(__filename, "- Error getting connection from pool", poolName);
					logger.error(err);
					return callBack(err);
				}
				return callBack(null, connection);
			});
		});
	}
	//return callBack(null,bannerpool);
}
