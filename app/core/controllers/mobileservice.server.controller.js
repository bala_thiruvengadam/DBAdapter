'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Mobile Service Controller:
//	Services the mobile service requests for GET, POST, DELETE etc and
//	returns the resulting data back to users
////////////////////////////////////////////////////////////////////////////////
var _ = require('lodash'),
	async = require('async'),
	logger = require('../utils/logger.server.util'),
	cache = require('../services/cache.server.services'),
	featureUtils = require('../../ui/utils/feature.server.util'),
	mobileUtils = require('../utils/mobile.server.util'),
	baseservice = require('../services/mobilebase.server.services');

////////////////////////////////////////////////////
//  create inputparams map
////////////////////////////////////////////////////
function createInputParamsMap(getParams, postParams, queryParams) {
	var inputParams = {};
	var getParamKeys, postParamKeys, queryParamKeys, getInputKeys;

	if (getParams) {
		getParamKeys = Object.keys(getParams);
	}
	if (postParams) {
		postParamKeys = Object.keys(postParams);
	}
	if (queryParams) {
		queryParamKeys = Object.keys(queryParams);
	}
	var i = 0,
		j = 0;
	if (getParamKeys && getParamKeys.length > 0) {
		for (i = 0; i < getParamKeys.length; i++) {
			if (getParamKeys[i] == "inputParams") {
				var pObj = splitParams(getParams[getParamKeys[i]]);
				getInputKeys = Object.keys(pObj);
				for (j = 0; j < getInputKeys.length; j++) {
					inputParams[getInputKeys[j]] = pObj[getInputKeys[j]];
				}
			} else {
				inputParams[getParamKeys[i]] = getParams[getParamKeys[i]];
			}
		}
	}
	if (postParamKeys && postParamKeys.length > 0) {
		for (i = 0; i < postParamKeys.length; i++) {
			inputParams[postParamKeys[i]] = postParams[postParamKeys[i]];
		}
	}
	if (queryParamKeys && queryParamKeys.length > 0) {
		for (i = 0; i < queryParamKeys.length; i++) {
			inputParams[queryParamKeys[i]] = queryParams[queryParamKeys[i]];
		}
	}
	return inputParams;
}

function splitParams(params) {
	// eg; termCode=201110&crn=10050
	var paramObj = {};
	var temp1 = params.split("&");
	_.forEach(temp1, function(param) {
		var temp2 = param.split("=");
		var tmpVal = (temp2.length <= 1) ? "" : temp2[1];
		paramObj[temp2[0]] = tmpVal;
	});
	return paramObj;
}

///////////////////////////////////////////////////////////////////////////////
function isServiceAvailableAndEnabled(module, config,
	callBack) {
	return callBack(null, module, config);
	/* We might not be using this feature */
	/*
	featureUtils.getCachedService(userPrincipal.tenantApp, module, function(err,
		value) {
		if (err || !value || !value.service) {
			var err = {
				statuscode: 200,
				message: "The Service is not defined."
			}
			return callBack(err);
		}
		if (!value.enabled) {
			var err = {
				statuscode: 200,
				message: "The Application is disabled."
			}
			return callBack(err);
		}
		var mRoles = value.roles,
			authorized = true;
		if (!mRoles || mRoles.length <= 0) {
			authorized = true;
		} else {
			if (userPrincipal.roles && userPrincipal.roles.length > 0) {
				_.forEach(userPrincipal.roles, function(role) {
					var findRole = _.find(mRoles, function(
						o) {
						return o == role
					});
					if (findRole) {
						authorized = true;
						return;
					}
				});
			}
		}
		if (!authorized) {
			var err = {
				statuscode: 200,
				message: "The User is not authorized to access this application."
			}
			return callBack(err);
		}
		return callBack(null, userPrincipal, module, config);
	});
	*/
}

/*
function getTenantConfig(userPrincipal, module, inputParams, callBack) {
	cache.getCachedObject(userPrincipal.tenantApp + '.config', function(err, config) {
		if (err) {
			return callBack(err);
		}
		return callback(null, userPrincipal, module, inputParams, config);
	});
}
*/

function getServiceModule(module, config, callBack) {
	mobileUtils.getServiceModule(module, config,
		function(err, module, config, moduleInfo) {
			// moduleInfo contains the service info
			if (err) {
				return callBack(err);
			}
			return callBack(null, moduleInfo);
		});
}

function checkRequestMethod(req, moduleInfo, callBack) {
	if (moduleInfo.method.toUpperCase() != req.method.toUpperCase()) {
		var err = {
			statuscode: 200,
			error: "The request method " + req.method + " for " + moduleInfo.name +
				" is not allowed. Allowed method is " + moduleInfo.method + "."
		}
		return callBack(err);
	}
	return callBack(null, moduleInfo);
}

function validateRequest(req, callBack) {
	var module = req.params.module,
		tenant = req.params.tenant,
		config = req.tenantConfig;
	async.waterfall([
		async.apply(isServiceAvailableAndEnabled, module,
			config),
		//getTenantConfig,  //now, config can be get through req.tenantConfig
		getServiceModule,
		async.apply(checkRequestMethod, req)
	], function(err, moduleInfo) {
		if (err) {
			return callBack(err);
		}
		logger.debug(__filename, "- Module meta data:");
		logger.debug(JSON.stringify(moduleInfo));
		return callBack(null, moduleInfo);
	});
}

function getInputParamsWithRemoteUser(userPrincipal, inputParams, moduleInfo,
	callBack) {
	inputParams["remoteUser"] = (userPrincipal.erpId) ? userPrincipal.erpId :
		userPrincipal.username;
	logger.debug(__filename, "Remote user is " + inputParams["remoteUser"]);
	return callBack(null, inputParams, moduleInfo);
}

function executeModule(config, userPrincipal, inputParams, moduleInfo,
	callBack) {
	baseservice.executeServiceModule(config, userPrincipal, inputParams,
		moduleInfo,
		function(err, results, userPrincipalUpdated) {
			return callBack(err, results, userPrincipalUpdated);
		});
}

function processRequest(req, inputParams, callBack) {
	var module = req.params.module,
		tenant = req.params.tenant,
		config = req.tenantConfig,
		userPrincipal = req.userPrincipal;
	logger.debug(__filename, "- Printing old userPrincipal:");
	logger.debug(userPrincipal);
	async.waterfall([
		async.apply(validateRequest, req),
		async.apply(getInputParamsWithRemoteUser, userPrincipal, inputParams),
		async.apply(executeModule, config, userPrincipal)
	], function(err, res, userPrincipalUpdated) {
		if (err) {
			err.statuscode = (err.statuscode) ? err.statuscode : 500;
			err.error = (err.error) ? err.error : "Internal server error";
			return callBack(err);
		}
		// Add new userPrincipal with pidm to cache
		userPrincipalUpdated.lastUpdated = new Date();
		logger.debug(__filename, "- Printing userPrincipalUpadted:");
		logger.debug(userPrincipalUpdated);
		//Add userPrincipal to Cache
		var cacheKey = userPrincipalUpdated.tenantApp + ".ticket." + req.query.ticket;
		cache.addToCache(cacheKey, userPrincipalUpdated, config, function(err,
			success) {
			if (err) {
				logger.error(__filename, "- Error: Module " + cacheKey + " couldn't be added to cache.");
				logger.error(err);
			}
			return;
		});
		/*
		if (userPrincipal && !userPrincipal.pidm && userPrincipalUpdated.pidm) {
			console.log("Updating userprincipal with pidm = " + userPrincipalUpdated.pidm);
			var cacheKey = userPrincipalUpdated.tenantApp + ".ticket." + req.query.ticket;
			cacheService.addToCache(cacheKey, userPrincipalUpdated, function(err,
				success) {
				if (err) {
					console.log("Error: Module " + cacheKey + " couldn't be added to cache.");
					console.log(err);
				}
				return callBack(null, userPrincipal, module, config, moduleMeta);
			});
		}
		*/
		return callBack(err, res);
	});
}
////////////////////////////////////////////////////////////////////////////
///  Global Controller functions
////////////////////////////////////////////////////////////////////////////
module.exports = {
	processGet: function processGetRequest(req, res) {
		var module = req.params.module,
			tenant = req.params.tenant,
			inputParams = createInputParamsMap(req.params, "", req.query),
			userPrincipal = req.userPrincipal;
		logger.debug(__filename, "- In processget:");
		logger.debug(userPrincipal);
		if (!userPrincipal) {
			res.writeHead(403, {
				'Content-Type': 'application/json'
			});
			res.end("Forbidden");
			return;
		}
		processRequest(req, inputParams, function(err, result) {
			if (err) {
				res.writeHead(err.statuscode, {
					'Content-Type': 'application/json'
				});
				res.end(err.error);
				return;
			}
			res.writeHead(200, {
				'Content-Type': 'application/json'
			});
			res.end(result);
			return;
		});
	},
	processGetWithParams: function processGetRequestWithParams(req, res) {
		var module = req.params.module,
			tenant = req.params.tenant,
			inputParams = createInputParamsMap(req.params, "", req.query);
		processRequest(req, inputParams, function(err, result) {
			if (err) {
				res.writeHead(err.statuscode, {
					'Content-Type': 'application/json'
				});
				res.end(err.error);
				return;
			}
			res.writeHead(200, {
				'Content-Type': 'application/json'
			});
			res.end(result);
			return;
		});
	},
	processPost: function processPostRequest(req, res) {
		//console.log(req.method + " : module=" + req.params.module + " : " + req.url);
		var module = req.params.module,
			tenant = req.params.tenant,
			inputParams = createInputParamsMap(req.params, req.body, req.query);
		processRequest(req, inputParams, function(err, result) {
			if (err) {
				res.writeHead(err.statuscode, {
					'Content-Type': 'application/json'
				});
				res.end(err.error);
				return;
			}
			res.writeHead(200, {
				'Content-Type': 'application/json'
			});
			res.end(result);
			return;
		});
	},
}
