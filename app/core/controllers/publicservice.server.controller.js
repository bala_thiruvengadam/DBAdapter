'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Mobile Service Controller:
//	Services the mobile service requests for GET, POST, DELETE etc and
//	returns the resulting data back to users
////////////////////////////////////////////////////////////////////////////////
var _ = require('lodash'),
  async = require('async'),
  cache = require('../services/cache.server.services'),
  featureUtils = require('../../ui/utils/feature.server.util'),
  mobileUtils = require('../utils/mobile.server.util'),
  baseservice = require('../services/mobilebase.server.services');

////////////////////////////////////////////////////
//  create inputparams map
////////////////////////////////////////////////////
function createInputParamsMap(getParams, postParams, queryParams) {
  var inputParams = {};
  var getParamKeys, postParamKeys, queryParamKeys, getInputKeys;

  if (getParams) {
    getParamKeys = Object.keys(getParams);
  }
  if (postParams) {
    postParamKeys = Object.keys(postParams);
  }
  if (queryParams) {
    queryParamKeys = Object.keys(queryParams);
  }
  var i = 0,
    j = 0;
  if (getParamKeys && getParamKeys.length > 0) {
    for (i = 0; i < getParamKeys.length; i++) {
      if (getParamKeys[i] == "inputParams") {
        var pObj = splitParams(getParams[getParamKeys[i]]);
        getInputKeys = Object.keys(pObj);
        for (j = 0; j < getInputKeys.length; j++) {
          inputParams[getInputKeys[j]] = pObj[getInputKeys[j]];
        }
      } else {
        inputParams[getParamKeys[i]] = getParams[getParamKeys[i]];
      }
    }
  }
  if (postParamKeys && postParamKeys.length > 0) {
    for (i = 0; i < postParamKeys.length; i++) {
      inputParams[postParamKeys[i]] = postParams[postParamKeys[i]];
    }
  }
  if (queryParamKeys && queryParamKeys.length > 0) {
    for (i = 0; i < queryParamKeys.length; i++) {
      inputParams[queryParamKeys[i]] = queryParams[queryParamKeys[i]];
    }
  }
  return inputParams;
}

function splitParams(params) {
  // eg; termCode=201110&crn=10050
  var paramObj = {};
  var temp1 = params.split("&");
  _.forEach(temp1, function(param) {
    var temp2 = param.split("=");
    var tmpVal = (temp2.length <= 1) ? "" : temp2[1];
    paramObj[temp2[0]] = tmpVal;
  });
  return paramObj;
}

///////////////////////////////////////////////////////////////////////////////



function getServiceModule(module, config, callBack) {
  mobileUtils.getServiceModule(module, config,
    function(err, module, config, moduleInfo) {
      // moduleInfo contains the service info
      if (err) {
        return callBack(err);
      }
      return callBack(null, moduleInfo);
    });
}

function checkRequestMethod(req, moduleInfo, callBack) {
  if (moduleInfo && moduleInfo.method.toUpperCase() != req.method.toUpperCase()) {
    var err = {
      statuscode: 200,
      error: "No module with the supplied name configured in query.xml"
    }
    return callBack(err);
  }
  //check if the module is secrured or not to access by public APIs
  if (moduleInfo && moduleInfo.secured) {
    var err = {
      statuscode: 200,
      error: "The request method " + req.method + " for " + moduleInfo.name +
        " is secured."
    }
    return callBack(err);
  }
  return callBack(null, moduleInfo);
}

function validateRequest(req, callBack) {
  var module = req.params.module,
    tenant = req.params.tenant,
    config = req.tenantConfig;
  async.waterfall([
    //getTenantConfig,  //now, config can be get through req.tenantConfig
    async.apply(getServiceModule, module, config),
    async.apply(checkRequestMethod, req)
  ], function(err, moduleInfo) {
    if (err) {
      return callBack(err);
    }
    console.log("module meta:");
    console.log(JSON.stringify(moduleInfo));
    return callBack(null, moduleInfo);
  });
}

function executeModule(config, inputParams, moduleInfo,
  callBack) {
  baseservice.executeServiceModule(config, null, inputParams,
    moduleInfo,
    function(err, results, userPrincipalVoid) {
      return callBack(err, results, userPrincipalVoid);
    });
}

function processRequest(req, inputParams, callBack) {
  var module = req.params.module,
    tenant = req.params.tenant,
    config = req.tenantConfig,
    userPrincipal = null;
  async.waterfall([
    async.apply(validateRequest, req),
    async.apply(executeModule, config, inputParams)
  ], function(err, res, userPrincipalVoid) {
    if (err) {
      err.statuscode = (err.statuscode) ? err.statuscode : 500;
      err.error = (err.error) ? err.error : "Internal server error";
      return callBack(err);
    }
    return callBack(err, res);
  });
}
////////////////////////////////////////////////////////////////////////////
///  Global Controller functions
////////////////////////////////////////////////////////////////////////////
module.exports = {
  processGet: function processGetRequest(req, res) {
    var module = req.params.module,
      tenant = req.params.tenant,
      inputParams = createInputParamsMap(req.params, "", req.query),
      userPrincipal = null;
    console.log("in processget:");

    processRequest(req, inputParams, function(err, result) {
      if (err) {
        res.writeHead(err.statuscode, {
          'Content-Type': 'application/json'
        });
        res.end(err.error);
        return;
      }
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.end(result);
      return;
    });
  },
  processGetWithParams: function processGetRequestWithParams(req, res) {
    var module = req.params.module,
      tenant = req.params.tenant,
      inputParams = createInputParamsMap(req.params, "", req.query);
    processRequest(req, inputParams, function(err, result) {
      if (err) {
        res.writeHead(err.statuscode, {
          'Content-Type': 'application/json'
        });
        res.end(err.error);
        return;
      }
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.end(result);
      return;
    });
  },
  processPost: function processPostRequest(req, res) {
    //console.log(req.method + " : module=" + req.params.module + " : " + req.url);
    var module = req.params.module,
      tenant = req.params.tenant,
      inputParams = createInputParamsMap(req.params, req.body, req.query);
    processRequest(req, inputParams, function(err, result) {
      if (err) {
        res.writeHead(err.statuscode, {
          'Content-Type': 'application/json'
        });
        res.end(err.error);
        return;
      }
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.end(result);
      return;
    });
  },
}
