var _ = require('lodash'),
  async = require('async'),
  request = require('request'),
  xmldom = require('elementtree'),
  logger = require('../utils/logger.server.util'),
  productConfigSchema = require('../../ui/models/ProductConfig.server.model'),
  cacheService = require('../services/cache.server.services'),
  baseservice = require('../services/mobilebase.server.services');

var kryptosServerAPIUrl = global.kryptosURL;
///////////////////////////////////////////////////////////////////////////////
// Functions specific for getTenantConfigFromDB
//////////////////////////////////////////////////////////////////////////////
function loadTenantConfigFromDB(tenantApp, callBack) {
  productConfigSchema.findByTenantApp(tenantApp, function(err, result) {
    if (err) {
      logger.error(__filename,
        "- In loadTenantConfigFromDB: Error loading tenant config from DB for ",
        tenantApp);
      logger.error(err);
      return callBack(err);
    }
    if (result) {
      result = JSON.parse(JSON.stringify(result));
    }
    return callBack(null, result);
  });
}

function addTenantConfigToCache(tenantApp, config, callBack) {
  cacheService.addToCache(tenantApp + ".config", config, '', function(err, success) {
    if (err) {
      return callBack(err);
    }
    return callBack(null, config, success);
  });
}

//////////////////////////////////////////////////////////////////////////////
function getModuleXMLMeta(module, config, callBack) {
  var reqUrl = kryptosServerAPIUrl + "gateway/" + config.tenant.name + "/" +
    module,
    tenantLicenseKey = config.licenseKey;
  request({
    url: reqUrl,
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'licenseKey': tenantLicenseKey
    }
  }, function(err, response, body) {
    if (err) {
      err.statuscode = 500;
      err.error =
        "Communication error with cloud configuration server.";
      return callBack(err);
    }
    if (response.statusCode == 200) {
      var isXMLOutput = true,
        dummyVar;
      try {
        dummyVar = JSON.parse(body);
        isXMLOutput = false;
      } catch (err) {
        //
      }
      if (isXMLOutput) {
        logger.debug(__filename, "- Module " + module + " meta info:");
        logger.debug(body);
        return callBack(null, module, config, body);
      } else {
        var err = {
          statuscode: 200,
          error: "Invalid License key."
        };
        return callBack(err);
      }
    } else {
      var err1 = {
        statuscode: 500,
        error: "Communication error with cloud configuration server."
      };
      return callBack(err1, null);
    }
    return callBack(null, module, config, body);
  });
}

function parseXMLMetaInfo(module, config, xmlDoc, callBack) {
  var doc;
  //xmlDoc = '<?xml version="1.0" encoding="utf-8"?> ' + xmlDoc.trim();
  // Replace the Byte Order Mark (BOM), a unicode character which Windows automatically
  // prepends to UTF8 filteredKeys
  xmlDoc = xmlDoc.replace("\ufeff", "");
  xmlDoc = xmlDoc.trim();
  try {
    doc = xmldom.parse(xmlDoc);
    logger.debug(__filename, "- printing xml doc:");
    logger.debug(doc);
    //return callBack(null, userPrincipal, module, config, doc);
  } catch (err) {
    logger.error(__filename, "Error parsing XML service metadata");
    logger.error(err);
    err.statuscode = 500;
    err.error =
      "Communication error with cloud configuration server.";
    return callBack(err);
  }
  return callBack(null, module, config, doc);
}

function constructModuleFromXMLDoc(module, config, doc, callBack) {
  var nodes, moduleMeta = {},
    sectionRow = {},
    paramRow = {};
  try {
    logger.debug(__filename, "- Root: ", doc.getroot());
    moduleMeta["name"] = doc.getroot().attrib["name"];
    moduleMeta["method"] = (doc.getroot().attrib["method"]) ? doc.getroot().attrib[
      "method"] : "GET";
    moduleMeta["sections"] = [];
    nodes = doc.findall("./section");
    _.forEach(nodes, function(node) {
      sectionRow = {}
      sectionRow["name"] = node.get("name");
      var subSects = node.getchildren();
      _.forEach(subSects, function(subSect) {
        var elTag = subSect.tag;
        switch (elTag) {
          case "invocationtype":
            sectionRow[elTag] = subSect.text;
            break;
          case "query":
            sectionRow[elTag] = subSect.text;
            break;
          case "parameters":
            sectionRow[elTag] = [];
            var params = subSect.getchildren();
            _.forEach(params, function(param) {
              paramRow = {};
              paramRow["key"] = param.get("key");
              paramRow["type"] = param.get("type");
              paramRow["category"] = (param.get("category")) ? param.get(
                "category") : "IN";
              paramRow["value"] = "";
              sectionRow[elTag].push(paramRow);
            });
            break;
          default:
            break;
        }
      });
      moduleMeta["sections"].push(sectionRow);
    });
  } catch (err) {
    logger.error(__filename, "- Error while extracting Module sections:");
    logger.error(err);
    err.statuscode = 500;
    err.error =
      "Communication error with cloud configuration server.";
    return callBack(err);
  }
  return callBack(null, module, config, moduleMeta);
}

function cacheModuleInfo(module, config, moduleMeta, callBack) {
  var cacheKey = config.appName + ".module." + module;
  cacheService.addToCache(cacheKey, moduleMeta, config, function(err, success) {
    if (err) {
      logger.error(__filename, "- Error: Module " + cacheKey + " couldn't be added to cache.");
      logger.error(err);
      //no need to return error as the service meta data is already there to execute
    }
    return callBack(null, module, config, moduleMeta);
  });
}

function getModuleFromCache(module, config, callBack) {
  var cacheKey = config.appName + ".module." + module;
  cacheService.getCachedObject(cacheKey, function(err, value) {
    if (err) {
      return callBack();
    }
    return callBack(null, module, config, value);
  });
}

function verifyCacheModule(module, config, cachedModule,
  callBack) {
  // if module exists in cache use that
  if (cachedModule && cachedModule.name) {
    return callBack(null, module, config, cachedModule);
  }
  async.waterfall([
    async.apply(getModuleXMLMeta, module, config),
    parseXMLMetaInfo,
    constructModuleFromXMLDoc,
    cacheModuleInfo
  ], function(err, module, config, moduleMeta) {
    return callBack(null, module, config, moduleMeta);
  });

}

///////////////////////////////////////////////////////////////////////////////

function sessionParams(config, cacheTicket, callBack) {
  callBack(null, config, cacheTicket);
}

function getRemoteUserPidm(tenant, module, inputParams, callBack) {
  baseservice.executeServiceModule(tenant, module, inputParams, function(err,
    result) {
    return callBack(err, result);
  });
}

function findAndRemoveDuplicateUserPrincipal(userPrincipal, fKey, callBack) {
  logger.debug(__filename, "- In function findAndRemoveDuplicateUserPrincipal:")
  logger.debug(__filename, "fKey:", fKey);
  cacheService.getCachedObject(fKey, function(err, cPrincipal) {
    if (err) {
      return callBack();
    }
    if (userPrincipal.tenantApp == cPrincipal.tenantApp &&
      userPrincipal.tenant == cPrincipal.tenant &&
      userPrincipal.username == cPrincipal.username) {
      // found matching userPrincipal object; remove from cache
      cacheService.removeFromCache(fKey, function(err, count) {
        logger.debug(__filename, " - ", count, " item removed from cache.");
        return callBack();
      });
    } else {
      return callBack();
    }
  });
}

module.exports = {
  updateCacheWithPidm: function updateCacheWithPidm(config, cacheTicket,
    userPrincipal, callBack) {
    if (!userPrincipal || !userPrincipal.username) {
      userPrincipal = cacheService.getCachedObject(cacheTicket);
    }
    if (userPrincipal && userPrincipal.pidm) {
      // Pidm already exist in cache: return
      return callBack(null, userPrincipal);
    } else if (userPrincipal && !userPrincipal.erpId) {
      // No ErpId to get Banner pidm
      return callBack(null, userPrincipal);
    } else if (!userPrincipal || !userPrincipal.username) {
      return callBack(null, userPrincipal);
    }
    var tenant = config.tenant;
    var module = "bannerpidmfromremoteuser";
    var inputParams = {}
    inputParams["remoteUser"] = userPrincipal.erpId;

    async.parallel({
      params: async.apply(sessionParams, config, cacheTicket, userPrincipal),
      getBannerMapping: async.apply(getRemoteUserPidm, tenant, module,
        inputParams)
    }, function(err, results) {
      // results.params & results.getBannerMapping
      //console.log(results);
      // update cache object with pidm

      return callBack(err, results.getBannerMapping);
    });
  },
  invalidateOldTicket: function invalidateOldTicket(userPrincipal, callBack) {
    //console.log("in function invalidateOldTicket");
    //console.log(JSON.stringify(userPrincipal));
    cacheService.listKeys(function(err, keys) {
      if (err) {
        return callBack(err);
      }
      var searchKey = userPrincipal.tenantApp + ".ticket.";
      // filter only for cache keys added for the specific tenant app
      var filteredKeys = _.filter(keys, function(o) {
        return o.indexOf(searchKey) >= 0;
      });
      //console.log("Filtered Keys from cache for the tenant app:");
      //console.log(filteredKeys);
      async.each(filteredKeys,
        findAndRemoveDuplicateUserPrincipal.bind(null, userPrincipal),
        function(err) {
          return callBack();
        });
    });
  },
  getServiceModule: function getServiceModule(module, config,
    callBack) {
    async.waterfall([
      async.apply(getModuleFromCache, module, config),
      verifyCacheModule
    ], function(err, module, config, moduleInfo) {
      if (err) {
        return callBack(err);
      }
      return callBack(null, module, config, moduleInfo);
    });
  },
  getTenantConfigFromDB: function getTenantConfigFromDB(tenantApp, callBack) {
    async.waterfall([
      async.apply(loadTenantConfigFromDB, tenantApp),
      addTenantConfigToCache
    ], function(err, tenantConfig, success) {
      if (err) {
        logger.error(__filename,
          "- In getTenantConfigFromDB: Error getting tenant config from DB:");
        logger.error(err);
        return callBack(err);
      }
      logger.debug(__filename, "- In getTenantConfigFromDB: Retrieved tenant config for ",
        tenantApp, " into cache.");
      logger.debug(tenantConfig);
      return callBack(null, tenantConfig);
    })
  }
}
