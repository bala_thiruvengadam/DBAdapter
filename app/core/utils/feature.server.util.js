"use strict";

var request = require('request'),
	xmldom = require('elementtree'),
	oracledb = require('oracledb'),
	_ = require('lodash');

var kryptosServerAPIUrl = global.kryptosURL; //   "https://kryptos.kryptosmobile.com/gateway/MWCC/";

/*var sampleSection = {
	section: "transcriptinfo",
	invocationtype: "SQL",
	query: "SELECT SPRIDEN_ID FROM SPRIDEN WHERE SPRIDEN_PIDM =1 AND SPRIDEN_CHANGE_IND IS NULL",
	parameters: [{
		name: "pidm",
		type: "NUMBER",
		category: "OUT"
	}]
}*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////
function retrieveModuleMetaInfo(tenant, module, inputparams, callBack) {
	request({
		url: kryptosServerAPIUrl + module,
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'licenseKey': tenantLicenseKey
		}
	}, function(err, response, body) {
		if (err) {
			console.log("Error retrieving module XML meta information: " + err);
			return callBack(err, null);
		}
		if (response.statusCode == 200) {
			return callBack(null, tenant, module, inputparams, body);
		} else {
			console.log("Error: couldn't retrieve XML meta information for the module");
			return callBack({
				"error": "Couldn't retrieve XML meta information for the module"
			}, null);
		}
	});
}

function parseXMLMetaInfo(tenant, module, inputparams, xmlDOc, callBack) {
	var doc, nodes;
	try {
		doc = xmldom.parse(xmlDoc);
		nodes = doc.findall("./section");
		return callBack(null, tenant, module, inputparams, nodes);
	} catch (err) {
		console.log("Error parsing XML service metadata");
		return callBack(err, null);
	}
}

function generateSections(tenant, module, inputparms, nodes, callBack) {
	var sqlSections = [];
	try {
		_.forEach(nodes, function(node) {
			var sectObj = {};
			sectObj.section = node.get("name");
			var subSects = node.getchildren();
			_.forEach(subSects, function(subSect) {
				var elTag = subSect.tag;
				switch (elTag) {
					case "invocationtype":
						sectObj.invocationtype = subSect.text;
						break;
					case "query":
						sectObj.query = subSect.text;
						break;
					case "parameters":
						sectObj.parameters = [];
						var params = subSect.getchildren();
						var pkey, ptype, pcategory;
						_.forEach(params, function(param) {
							pkey = param.get("key");
							ptype = param.get("type");
							pcategory = param.get("category");
							if (!pcategory || pcategory == undefined) {
								pcategory = "IN";
							}
							if (pcategory == "OUT") {
								sectObj.parameters.push({
									"key": pkey,
									"type": ptype,
									"category": pcategory,
									"dir": oracledb.BIND_OUT,
									"value": ""
								});
							} else {
								sectObj.parameters.push({
									"key": pkey,
									"type": ptype,
									"category": pcategory,
									"value": ""
								});
							}
						});
						break;
					default:
						break;
				}
			});
			if (sectObj) {
				sqlSections.push(sectObj);
			}
		});
		return callBack(null, tenant, module, inputparms, sqlSections);
	} catch (err) {
		console.log("Error generating XML meta sections");
		return callBack(err, null);
	}
}


exports.retrieveSQLSectionsFromModule = function(tenant, module, inputparams,
	callBack) {
	async.waterfall([
			async.apply(retrieveModuleMetaInfo, tenant, module, inputparams),
			parseXMLMetaInfo,
			generateSections
		],
		function(err, tenant, module, inputparams, sections) {
			callBack(err, tenant, module, inputparams, sections);
		});
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////

function retrieveFeatures(tempStore, callBack) {
	var module = tempStore.module,
		tenantLicenseKey = tempStore.config.licenseKey,
		tenant = tempStore.config.tenant.name;
	try {
		request({
			url: kryptosServerAPIUrl + 'gateway/' + tenant,
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'licenseKey': tenantLicenseKey
			}
		}, function(err, response, body) {
			if (err) {
				err.statuscode = 500;
				err.error = "Communication error with cloud configuration server.";
				return callBack(err);
			}
			if (response.statusCode == 200) {
				if (response.licenseresponse && !response.licenseresponse.validity) {
					var err = {
						statuscode: 200,
						error: "Invalid License key."
					};
					return callBack(err);
				} else {
					return callBack(null, tempStore, body);
				}
			} else {
				var err = {
					statuscode: 500,
					error: "Communication error with cloud configuration server."
				};
				return callBack(err, null);
			}
		});
	} catch (err) {
		err.statuscode = 500;
		err.error = "Communication error with cloud configuration server.";
		return callBack(err);
	}
}

function cacheFeatures(tempStore, features, callBack) {
	//
}

exports.populateFeatures = function(tempStore) {
	var config = tempStore.config;
	async.waterfall([
		async.apply(retrieveFeatures, tempStore),
		cacheFeatures
	], function(err) {

	});
}

/*
exports.retrieveSQLSectionsFromModule = function(module, inputparams, callback) {
	sqlSections = [];
	request({
		url: kryptosServerAPIUrl + module,
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'licenseKey': tenantLicenseKey
		}
	}, function(err, response, body) {
		if (err) {
			console.log(err);
			return callback(err, null);
		}
		if (response.statusCode == 200) {
			var doc = "";
			var nodes = "";
			try {
				doc = xmldom.parse(body);
				nodes = doc.findall("./section");

				var i = 0,j = 0,k = 0;
				for (i = 0; i < nodes.length; i++) {
					sqlSections[i] = {};
					//console.log("-----------------------------------------");
					//console.log("Section Name: " + nodes[i].get('name'));
					sqlSections[i].section = nodes[i].get('name');
					var subSects = nodes[i].getchildren();
					for (j = 0; j < subSects.length; j++) {
						var elTag = subSects[j].tag;
						if (elTag == "invocationtype") {
							sqlSections[i].invocationtype = subSects[j].text;
							//console.log("Element: " + elTag);
							//console.log("Text: "+ subSects[j].text);
						} else if (elTag == "query") {
							sqlSections[i].query = subSects[j].text;
							//console.log("Element: " + elTag);
							//console.log("Text: "+ subSects[j].text);
						} else if (elTag == "parameters") {
							//console.log("Element: " + elTag);
							sqlSections[i].parameters = [];
							var params = subSects[j].getchildren();
							for (k = 0; k < params.length; k++) {
								var pkey = params[k].get("key");
								var ptype = params[k].get("type");
								var pcategory = params[k].get("category");
								if (!pcategory || pcategory == undefined) {
									pcategory = "IN";
								}
								if (pcategory == "OUT") {
									sqlSections[i].parameters.push({
										"key": pkey,
										"type": ptype,
										"category": pcategory,
										"dir": oracledb.BIND_OUT,
										"value": ""
									});
								} else {
									sqlSections[i].parameters.push({
										"key": pkey,
										"type": ptype,
										"category": pcategory,
										"value": ""
									});
								}

								//console.log("{ key : " + pkey + " , type : "+ ptype + " , category : "+ pcategory);
							}
						}
					}
				}
				//console.log(sqlSections.length);
				//console.log(sqlSections);
			} catch (ex) {
				console.log(ex);
				callback(ex, null);
				return;
			}
			callback(null, sqlSections, inputparams);
			return;
		} else {
			callback(null, null)
			return;
		}
	});
}
*/
