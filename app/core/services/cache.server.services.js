var _ = require('lodash'),
	async = require('async');
const NodeCache = require('node-cache');
const cache = new NodeCache();
var logger = require('../utils/logger.server.util');
var cacheModel = require('../../ui/models/cache.server.model');
///////////////////////////////////////////////////////////////////////////////////////////////////
// The static cache would be placed directly to memory whereas the dynamic cache may be placed
// to memory or to DB based on the cacheSource property set on the product configuration.
///////////////////////////////////////////////////////////////////////////////////////////////////
var staticCacheKeyFormats = ['.config'];
var dynamicCacheKeyFormats = ['.ticket.', '.module.'];


function getCacheKeyValue(mkey, callBack) {
	cache.get(mkey, function(err, value) {
		var result;
		try {
			result = JSON.parse(value);
		} catch (err) {
			result = value;
		}
		var cObj = {};
		cObj[mkey] = result;
		return callBack(err, cObj);
	});
}

function reportCacheAsJSON(callBack) {
	cache.keys(function(err, mykeys) {
		if (err) {
			return callBack(err);
		}
		async.map(mykeys,
			getCacheKeyValue,
			function(err, outRes) {
				var formattedResObj = {},
					formatCounter = 0,
					resKeys;
				for (formatCounter = 0; formatCounter < outRes.length; formatCounter++) {
					resKeys = Object.keys(outRes[formatCounter]);
					var resKeyText = resKeys[0];
					formattedResObj[resKeyText] = outRes[formatCounter][resKeyText];
				}
				console.log("Cache Results:");
				console.log(JSON.stringify(formattedResObj));
				return callBack(null, formattedResObj);
			});
	});
}

function extractTenantAppName(key, callBack) {
	var tenantApp = "",
		skip = true;
	logger.info("Key=", key);
	var criteriaFound = _.find(dynamicCacheKeyFormats, function(o) {
		return (key.indexOf(o) >= 0);
	});
	if (criteriaFound) {
		tenantApp = key.substr(0, key.indexOf("."));
		skip = false;
	}
	logger.debug(__filename, "- in ExtractTenantAppName: tenantApp:", tenantApp);
	return callBack(null, key, tenantApp, skip);
}

function getTenantAppConfig(key, appName, skip, callBack) {
	if (skip) {
		logger.debug(__filename, "- in getTenantAppConfig: Skipping retrieving tenant config from cache.");
		return callBack(null, key, null);
	}
	var searchKey = appName + ".config";
	cache.get(searchKey, function(err, value) {
		if (err || !value) {
			return callBack({
				"statuscode": 500,
				"error": "Tenant not found."
			});
		}
		logger.debug(__filename, "- In getTenantAppConfig: retrieved tenant config from cache:");
		logger.debug(value);
		return callBack(null, key, value);
	});
}

function retrieveCacheInfo(key, config, callBack) {
	if (config && config.cacheSource == "DB") {
		// Retrieve the cache info from DB
		logger.debug(__filename, "- In retrieveCacheInfo: Getting cached object from DB");
		cacheModel.findCache(key, function(err, result) {
			if (err) {
				return callBack(err);
			}
			if (result) {
				logger.debug(__filename, "- In retrieveCacheInfo: Got cache object from DB:");
				logger.debug(JSON.stringify(result));
			}
			return callBack(null, (result ? result.data : result));
		});
	} else {
		// Retrieve the cache info from memory
		logger.debug(__filename, "- In retrieveCacheInfo: Getting cached object from Memory");
		cache.get(key, function(err, value) {
			if (err) {
				logger.debug(__filename,
					"- In retrieveCacheInfo: Error retrieving cached object from memory:")
				logger.debug(err);
				return callBack(err);
			}
			logger.debug(__filename, "- In retrieveCacheInfo: Got cache object from Cache:")
			logger.debug(value);
			return callBack(null, value);
		});
	}
}

function removeCacheInfo(key, config, callBack) {
	if (config && config.cacheSource == "DB") {
		// Remove the cache info from DB
		logger.debug(__filename, "- In removeCacheInfo: Removing cached object from DB");
		cacheModel.removeFromCache(key, function(err, removed) {
			if (err) {
				return callBack(err);
			}
			return callBack(null, removed);
		});
	} else {
		// Remove the cache info from memory
		logger.debug(__filename, "- In removeCacheInfo: Removing cached object from Memory");
		cache.del(key, function(err, count) {
			if (err) {
				return callBack(err);
			}
			return callBack(null, count);
		});
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////
module.exports = {
	addToCache: function addToCache(key, value, config, callBack) {
		logger.debug(__filename, "- In addToCache: Key=", key, "- Type of value=", typeof value,
			"- Value=");
		if (!key) {
			return callBack({
				"statuscode": 500,
				"error": "Key is not entered."
			});
		}
		if (typeof value == "object") {
			logger.debug(JSON.parse(JSON.stringify(value)));
		} else {
			logger.debug(value);
		}
		value = (typeof value == "object") ? JSON.parse(JSON.stringify(value)) : value;
		if (config && config.cacheSource == "DB") {
			logger.debug(__filename, "- In addToCache: Adding cache information to DB.");
			cacheModel.addtoCache(key, value, function(err, cacheRecord) {
				if (err) {
					logger.error("error while saving cache record to DB:");
					logger.error(err);
					return callBack(err);
				}
				logger.debug("Successfully saved the cache record to DB.");
				logger.debug(JSON.stringify(cacheRecord));
				return callBack(null, cacheRecord);
			});
		} else {
			logger.debug(__filename, "- In addToCache: Adding cache information to memory.");
			cache.set(key, value, function(err, success) {
				if (err) {
					logger.error("error while saving cache record to memory:");
					logger.error(err);
					return callBack(err);
				}
				logger.debug("Successfully saved the cache record to memory.");
				return callBack(null, success);
			});
		}
	},
	getCachedObject: function getCachedObject(key, callBack) {
		logger.debug(__filename, "- In getCachedObject: Key=", key);
		if (!key) {
			return callBack({
				"statuscode": 500,
				"error": "Key is not entered."
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		async.waterfall([
			async.apply(extractTenantAppName, key),
			getTenantAppConfig,
			retrieveCacheInfo
		], function(err, result) {
			if (err) {
				logger.error("Error while retrieving cache object:");
				logger.error(err);
				return callBack(err);
			}
			logger.debug(__filename, "- In getCachedObject: Got cached object for key ", key, " - ");
			logger.debug(result);
			return callBack(null, result);
		});
		////////////////////////////////////////////////////////////////////////////////////////
	},
	listCache: function listCache(callBack) {
		reportCacheAsJSON(function(err, results) {
			return callBack(null, results);
		});
	},
	removeFromCache: function removeFromCache(key, callBack) {
		logger.info(__filename, "- in removeFromCache: key=", key);
		if (!key) {
			return callBack({
				"statuscode": 500,
				"error": "Key is not entered."
			});
		}
		////////////////////////////////////////////////////////////////////////////////////////
		async.waterfall([
			async.apply(extractTenantAppName, key),
			getTenantAppConfig,
			removeCacheInfo
		], function(err, result) {
			if (err) {
				logger.error("Error while retrieving cache object:");
				logger.error(err);
				return callBack(err);
			}
			logger.debug(__filename, "- In removeFromCache: Got cached object for key ", key, " - ");
			logger.debug(result);
			return callBack(null, result);
		});
		////////////////////////////////////////////////////////////////////////////////////////

	},
	removeFromCacheForKeysStartingwith: function removeFromCacheForKeysStartingwith(
		str, callBack) {
		cache.keys(function(err, mykeys) {
			if (err) {
				return callBack(err);
			}
			var fKeys = _.filter(mykeys, function(o) {
				return (o.substr(0, str.length) === str) ? true : false;
			});
			cache.del(fKeys, function(err, count) {
				if (!err) {
					//console.log(count + " number of keys starting with " + str +
					//	" cleared from cache.");
				}
				return callBack();
			});
		});
	},
	listKeys: function listKeys(callBack) {
		cache.keys(function(err, mykeys) {
			if (err) {
				return callBack(err);
			}
			return callBack(null, mykeys);
		});
	},
	filterKeysStartingWith: function filterKeysStartingWith(str, callBack) {
		cache.keys(function(err, mykeys) {
			if (err) {
				return callBack(err);
			}
			var fKeys = _.filter(mykeys, function(o) {
				//return o.startsWith(str);
				//console.log("startswith key =" + o.substr(0, str.length) + " - compare str=" + str);
				return (o.substr(0, str.length) === str);
			});
			return callBack(null, fKeys);
		});
	},
	filterKeysContainString: function filterKeysContainString(str, callBack) {
		cache.keys(function(err, mykeys) {
			if (err) {
				return callBack(err);
			}
			var fKeys = _.filter(mykeys, function(o) {
				//return o.startsWith(str);
				//console.log("contain str index =" + o.indexOf(".ticket.") + " - compare str=" + str);
				return (o.indexOf(".ticket.") >= 0);
			});
			return callBack(null, fKeys);
		});
	}
}
