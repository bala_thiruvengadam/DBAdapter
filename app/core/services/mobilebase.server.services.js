'use strict';

var oracledb = require('oracledb'),
	_ = require('lodash'),
	async = require('async'),
	logger = require('../utils/logger.server.util'),
	cache = require('../services/cache.server.services'),
	features = require('../utils/feature.server.util'),
	mobileUtils = require('../utils/mobile.server.util'),
	dbexecutor = require('../dao/dbexecutor.server.dao');

var bannerPidmFromRemoteUser = "bannerpidmfromremoteuser",
	getPidmfromUdcid = "getpidmfromudcid",
	spridenPidmColumnName = "SPRIDEN_PIDM",
	pidmColumnName = "GOBUMAP_PIDM";


function getOracledbType(serviceDataType) {
	//console.log("in getOracledbType");
	var dbType = "";
	switch (serviceDataType) {
		case "NUMBER":
			dbType = oracledb.NUMBER;
			break;
		case "VARCHAR":
			dbType = oracledb.STRING;
			break;
		case "CLOB":
			dbType = oracledb.CLOB;
			break;
		case "CURSOR":
			dbType = oracledb.CURSOR;
			break;
		case "DATE":
			dbType = oracledb.DATE;
			break;
		default:
			dbType = oracledb.DEFAULT;
			break;
	}
	return dbType;
}

function buildParameterMap(params) {
	//console.log("in buildParameterMap");
	if (!params || params.length <= 0) {
		return {};
	}
	var paramObj = "";
	paramObj = "{ ";
	_.forEach(params, function(parm) {
		paramObj += (paramObj == "{ ") ? "" : ", ";
		if (parm.category == "IN") {
			paramObj += ' "' + parm.key + '" : "' + parm.value + '" ';
		} else {
			var oraDbType = getOracledbType(parm.type);
			paramObj += ' "' + parm.key + '" : ' +
				' { "dir" : ' + oracledb.BIND_OUT +
				', "type" : ' + oraDbType +
				'  } ';
		}
	});
	paramObj += " }";
	//console.log("in buildParameterMap: paramObj=");
	//console.log(paramObj);
	return JSON.parse(paramObj);
}

///////////////////////////////////////////////////////////////////////////////
//	functions to retrieve user pidm value
///////////////////////////////////////////////////////////////////////////////
function getServiceModule(module, config, callBack) {
	mobileUtils.getServiceModule(module, config,
		function(err, module, config, moduleInfo) {
			//console.log("getuserpidm service:");
			//console.log(JSON.stringify(moduleInfo));
			// moduleInfo contains the service info
			return callBack(err, moduleInfo);
		});
}

function setupParameters(userPrincipal, inputParams, moduleInfo, callBack) {
	var sections = moduleInfo.sections;
	return callBack(null, sections, userPrincipal, inputParams);
}

function executeSectionForUserPidm(config, userPrincipal, module, inputParams,
	callBack) {
	async.waterfall
	([
		async.apply(getServiceModule, module, config),
		async.apply(setupParameters, userPrincipal, inputParams),
		replaceParameterValues,
		async.apply(executeSections, config)
	], function(err, results) {
		//console.log("results for pidm fetch:");
		//console.log(JSON.stringify(results));
		return callBack(err, results);
		// add the pidm parameter value to inputParams at the end
		//inputParams["pidm"] = userPrincipal.pidm;
	});
}

function getPidmFromRemoteUser(config, userPrincipal, inputParams, callBack) {
	// If pidm already in userPrincipal
	if (userPrincipal && userPrincipal.pidm) {
		return callBack(null, userPrincipal.pidm);
	}
	if (!inputParams["remoteUser"]) {
		var err = {
			"statuscode": 200,
			"error": "UDCId is null or empty. Could not get UDCID from the ticket"
		}
		return callBack(err);
	}
	// else, execute the pidm service to get the pidm value
	executeSectionForUserPidm(config, userPrincipal, bannerPidmFromRemoteUser,
		inputParams,
		function(err, results) {
			if (err) {
				err = {
					"statuscode": 200,
					"error": "Could not create query for getting pidm from udcId. Invalid configuration in query.xml"
				}
				return callBack(err);
			}
			err = {};
			if (!results["getpidmfromremoteuser"] ||
				results["getpidmfromremoteuser"].length == 0 ||
				(!results["getpidmfromremoteuser"][0].SPRIDEN_PIDM) ||
				(!results["getpidmfromremoteuser"][0].SPRIDEN_PIDM ==
					"unknown")) {
				err = {
					"statuscode": 200,
					"error": "Could not get PIDM for the supplied remoteuser"
				}
				return callBack(err);
			}
			var pidmOrUDCID = results["getpidmfromremoteuser"][0].SPRIDEN_PIDM;
			//console.log("pidmOrUDCID=" + pidmOrUDCID);
			if (isNaN(Number(pidmOrUDCID))) {
				//get pidm from UDCID
				//console.log("Pidm not a number; getting pidm from UDCID " + pidmOrUDCID);
				inputParams["udcId"] = pidmOrUDCID;
				executeSectionForUserPidm(config, userPrincipal, getpidmfromudcid,
					inputParams,
					function(err, results) {
						if (err) {
							err = {
								"statuscode": 200,
								"error": "Could not create query for getting pidm from udcId. Invalid configuration in query.xml"
							}
							return callBack(err);
						}
						if (!results["getpidmfromudcid"] ||
							results["getpidmfromudcid"].length == 0 ||
							(!results["getpidmfromudcid"][0].GOBUMAP_PIDM)
							(isNaN(results["getpidmfromudcid"][0].GOBUMAP_PIDM))) {
							err = {
								"statuscode": 200,
								"error": "Could not get PIDM for the supplied UDCID"
							}
							return callBack(err);
						}
						userPrincipal.pidm = Number(results["getpidmfromudcid"][0].GOBUMAP_PIDM);
						inputParams["PIDM"] = userPrincipal.pidm;
						return callBack(null, userPrincipal, inputParams);
					});
			} else {
				userPrincipal.pidm = Number(pidmOrUDCID);
				inputParams["PIDM"] = userPrincipal.pidm;
				//console.log("PIDM got userPrincipal updated:");
				//console.log(JSON.stringify(userPrincipal));
				return callBack(null, userPrincipal, inputParams);
			}
		});
}
///////////////////////////////////////////////////////////////////////////////


function isPidmParameterExists(sections) {
	var existsPidm = false;
	_.forEach(sections, function(section) {
		var params = section.parameters;
		if (params && params.length > 0) {
			var pidmParams = _.filter(params, function(o) {
				return (o.key == "PIDM" || o.key == "pidm") && o.category.toUpperCase() == "IN";
			});
			if (pidmParams && pidmParams.length > 0) {
				existsPidm = true;
				return;
			}
		}
	});
	return existsPidm;
}

function isPidmInUserPrincipal(userPrincipal) {
	return (userPrincipal && userPrincipal.pidm) ? true : false;
}

function checkForPidm(config, userPrincipal, sections, inputParams, callBack) {
	var existsPidm = false,
		requirePidm = false;
	if (isPidmParameterExists(sections)) {
		if (isPidmInUserPrincipal(userPrincipal)) {
			existsPidm = true;
			inputParams["PIDM"] = userPrincipal.pidm;
			//console.log("checkForPidm: user pidm already exists in cache!");
		}
		requirePidm = !existsPidm;
	} else {
		//console.log("checkForPidm: pidm not required for sections ");
		requirePidm = false;
	}
	if (requirePidm) {
		//console.log(
		//	"checkForPidm: calling getPidmFromRemoteUser for retrieving user pidm from database"
		//);
		// Retrieve user pidm from Banner
		getPidmFromRemoteUser(config, userPrincipal, inputParams, function(err,
			userPrincipal, inputParams) {
			//console.log("results for getPidmFromRemoteUser");
			//console.log(JSON.stringify(userPrincipal));
			//console.log(inputParams);
			return callBack(err, userPrincipal, inputParams);
		});
	} else {
		return callBack(null, userPrincipal, inputParams);
	}
}

function replaceParameterValues(sections, userPrincipal, inputParams, callBack) {
	//console.log("in replaceParameterValues");
	_.forEach(sections, function(section) {
		//var params = section.parameters;
		if (section.parameters && section.parameters.length > 0) {
			_.forEach(section.parameters, function(parm) {
				if (inputParams[parm.key]) {
					parm.value = inputParams[parm.key];
					//console.log("key=" + parm.key + " - value=" + inputParams[parm.key]);
				} else if (inputParams[parm.key.toUpperCase()]) {
					parm.value = inputParams[parm.key.toUpperCase()];
					//console.log("key=" + parm.key + " - value=" + inputParams[parm.key.toUpperCase()]);
				}
			});
			//console.log("In getParameterValues: params=");
			//console.log(section.parameters);
		}
	});
	return callBack(null, sections, userPrincipal, inputParams);
}

function executeSections(config, sections, userPrincipal, inputParams, callback) {
	//console.log("in executeSections");
	var sectionName,
		invocationType,
		params = [],
		resultMap = {};
	//console.log("Section length = " + sections.length);
	_.forEach(sections, function(section) {
		sectionName = section.name;
		invocationType = section.invocationtype;
		params = section.parameters;
		section["parameterObject"] = {};
		var parameterObject = "";
		if (params) {
			parameterObject = buildParameterMap(params);
			section["parameterObject"] = parameterObject;
		}
	});
	dbexecutor(config, sections, function(err, result) {
		//console.log("after dbexecute:");
		if (err) {
			logger.error(__filename, "ERROR/: " + err);
		} else {
			//console.log("Results:");
			//console.log(result);
			logger.info(__filename, "Execution results:", result);
		}
		callback(err, result, userPrincipal);
	});
}

function createTempSectionForTesting(config, callBack) {
	var sections = [],
		section = {};
	section.name = "testconnection";
	section.invocationtype = "SQL";
	section.query = config.dataSourceConfig.testSQL;
	if (!section.query) {
		section.query = "select 1 from dual";
	}
	section["parameterObject"] = {};
	sections.push(section);
	return callBack(null, config, sections);
}

function executeTempSection(config, sections, callBack) {
	dbexecutor(config, sections, function(err, result) {
		//console.log("after dbexecute:");
		if (err) {
			return callBack(err);
		}
		callBack(null, result);
	});
}

//////////////////////////////////////////////////////////////////////////
/// Global service functions
//////////////////////////////////////////////////////////////////////////
exports.executeServiceModule = function(config, userPrincipal, inputParams,
	moduleInfo, callBack) {
	var sections = moduleInfo.sections;
	async.waterfall([
		async.apply(checkForPidm, config, userPrincipal, sections, inputParams),
		async.apply(replaceParameterValues, sections),
		async.apply(executeSections, config)
	], function(err, result, userPrincipalUpdated) {
		if (err) {
			return callBack(err);
		}
		return callBack(null, JSON.stringify(result), userPrincipalUpdated);
	});

}

exports.testDBConnection = function(config, callBack) {
	logger.info(__filename, " - Testing DB connectivity...", config.dataSourceConfig);
	async.waterfall([
		async.apply(createTempSectionForTesting, config),
		executeTempSection
	], function(err, result) {
		if (err) {
			logger.error(__filename, " DB connection error:", err);
			return callBack(err);
		}
		logger.info(__filename, result);
		return callBack(null, JSON.stringify(result));
	})
}
