// CAS Authentication
var request = require('request'),
	_ = require('lodash'),
	async = require('async'),
	et = require('elementtree'),
	logger = require('../utils/logger.server.util'),
	principal = require('../models/UserPrincipal');
//XRegExp = require('xregexp');

function getTicketGrantingTicket(tempStore, callback) {
	var tgtUrl = "";
	var config = tempStore.config.authConfig,
		casUrl = config.casRootContext + "/v1/tickets",
		username = tempStore.username,
		password = tempStore.password;
	request({
		url: casUrl,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: 'username=' + username + '&password=' + password,
		timeout: 5000
	}, function(err, response, body) {
		tgtUrl = "";
		if (err) {
			logger.error(__filename, " - Error: " + err);
			err = {
				"statuscode": 500,
				"error": err.message
			}
			callback(err);
			return;
		}
		//console.log("Status Code=" + response.statusCode);
		//console.log(response.headers);
		if (response.statusCode == 201) {
			// Successful authentication to CAS
			tgtUrl = response.headers['location'];
			//console.log("Location=" + tgtUrl);
			var slashIndex = tgtUrl.lastIndexOf("/");
			if (slashIndex > 1) {
				tgtUrl = tgtUrl.substring(slashIndex + 1, tgtUrl.length);
				//console.log("TGT Ticket=" + tgtUrl);
				return callback(null, tempStore, tgtUrl);
			} else {
				err = {
					"statuscode": 500,
					"error": "Internal server error"
				}
				return callback(err);
			}
		} else if (response.statusCode == 400) {
			// Bad request
			err = {
				"statuscode": 200,
				"error": "Invalid username / password"
			};
			return callback(err);
		} else {
			err = {
				"statuscode": 500,
				"error": "Internal server error"
			};
			return callback(err);
		}
	});
}

function getServiceTicket(tempStore, tgt, callback) {
	//console.log("printing tempstore");
	//console.log(JSON.stringify(tempStore));
	var config = tempStore.config.authConfig;
	var casRoot = config.casRootContext;
	var casServer = casRoot + "/v1/tickets/" + tgt;
	if (config && !config.casRoleXpath) {
		return callback(null, tempStore, '');
	}
	request({
		url: casServer,
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: 'service=' + casRoot,
		timeout: 6000
	}, function(err, response, body) {
		if (err) {
			logger.error(__filename, " - ", err);
			err = {
				"statuscode": 500,
				"error": "Internal server error"
			}
			return callback(err);
		}
		//console.log(response.statusCode);
		//console.log(body);
		if (response.statusCode == 200) {
			// Got the successful service ticket
			//getSamlResponse(body);
			return callback(null, tempStore, body);
		} else {
			err = {
				"statuscode": 200,
				"error": "Error " + response.statsCode + " : " + response.body
			}
			return callback(err);
		}
		//return;
	});
}

function getSamlResponse(tempStore, serviceTicket, callBack) {
	var config = tempStore.config.authConfig;
	if (config &&
		(!config.casRoleXpath && !config.casErpidXpath)) {
		return callBack(null, tempStore, '');
	} else {
		if (!serviceTicket) {
			return callBack(null, tempStore, '');
		}
		// Get the SAML response to identify roles and erpid
		//console.log("------------------------------------------------------------------------------");
		//console.log("calling SAML request " + config.casRootContext + '/samlValidate?ticket=' +
		//	serviceTicket);
		//console.log("------------------------------------------------------------------------------");
		logger.info(__filename, " - Calling SAML request ", config.casRootContext +
			'/samlValidate?ticket=' + serviceTicket);
		request({
			url: config.casRootContext + '/samlValidate?ticket=' + serviceTicket,
			method: 'POST',
			headers: {
				'Content-Type': 'application/xml'
			},
			body: '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"><SOAP-ENV:Header/><SOAP-ENV:Body><samlp:Request xmlns:samlp="urn:oasis:names:tc:SAML:1.0:protocol" MajorVersion="1" MinorVersion="1" RequestID="_192.168.16.51.1024506224022" IssueInstant="2002-06-19T17:03:44.022Z"><samlp:AssertionArtifact>' +
				serviceTicket +
				'</samlp:AssertionArtifact></samlp:Request></SOAP-ENV:Body></SOAP-ENV:Envelope>'
		}, function(err, response, body) {
			if (err) {
				console.log(err);
				return callBack(err);
			}
			//console.log(response.statusCode);
			//console.log(body);
			if (response.statusCode == 200) {
				//getSAMLDocument(body);
				return callBack(null, tempStore, body);
			} else {
				logger.error(__filename,
					" - Error occured while retrieving the SAML response from CAS server");
				return callBack(null, tempStore, '');
			}
		});
	}
}

function getSAMLDocument(tempStore, samlResponse, callBack) {
	var doc = "";
	var config = tempStore.config.authConfig;
	if (!samlResponse || (!config.casRoleXpath && !config.casErpidXpath)) {
		return callBack(null, tempStore, '');
	}
	try {
		// Parse XML from string response
		//doc = new xmldom().parseFromString(samlResponse);
		doc = et.parse(samlResponse);
		//console.log("SAML response parsed successfully !");
		//console.log(doc.toString({preserverWhitespace : true}));
		return callBack(null, tempStore, doc);
		/*if (config.casRoleXpath) {
			getUserRoles(doc);
		} else if (config.casErpidXpath) {
			//getUserErpId(doc);
		}
		*/
	} catch (ex) {
		logger.error(__filename, " - Error when parsing SAML response to XML document, error: " + ex);
		return callBack({
			"statuscode": 500,
			"error": "Internal Server Error"
		});
	}
}

function getUserRoles(tempStore, samlDoc, callBack) {
	var config = tempStore.config.authConfig;
	if (!samlDoc || !config.casRoleXpath) {
		return callBack(null, tempStore, samlDoc, '');
	} else {
		var nodes = "";
		try {
			// get the filtered nodes
			var nodes = samlDoc.findall("*" + config.casRoleXpath);
			var activeRoles = [];
			for (var i = 0; i < nodes.length; i++) {
				//console.log(nodes[i].text);
				var roleGroup = nodes[i].text;
				//console.log(roleGroup);
				if (config.casRoleExtractGroup && roleGroup) {
					//casRoleExtractGroup
					if (roleGroup.toLowerCase().indexOf(config.casRoleExtractGroup.toLowerCase()) >= 0) {
						var regExp = new RegExp('(' + config.casRoleExtractGroup + ')', 'gi');
						roleGroup = roleGroup.replace(regExp, '');
						roleGroup = roleGroup ? roleGroup.trim() : '';
						if (roleGroup && roleGroup.substr(roleGroup.length - 1) == ',') {
							roleGroup = roleGroup.substr(0, roleGroup.length - 1);
						}
						//casRoleExtractRole
						if (roleGroup && config.casRoleExtractRole) {
							regExp = new RegExp('(' + config.casRoleExtractRole + ')', 'gi');
							roleGroup = roleGroup.replace(regExp, '');
							roleGroup = (roleGroup.length > 1 && roleGroup.substr(0, 1) == '=') ? roleGroup.substr(1) :
								roleGroup;
						}
						var rolesG = roleGroup.split(',');
						_.forEach(rolesG, function(iRole) {
							activeRoles.push(iRole);
						});
					}
				}
			}
			return callBack(null, tempStore, samlDoc, activeRoles);
		} catch (ex) {
			logger.error(__filename, ' - Error filtering cas.role.xpath from SAML doc. Error: ' + ex);
			return callBack(null, tempStore, samlDoc, '');
		}
	}
}

function getUserErpId(tempStore, samlDoc, activeRoles, callBack) {
	var config = tempStore.config.authConfig;
	if (!samlDoc || !config.casErpidXpath) {
		return callBack(null, tempStore, activeRoles, '');
	} else {
		var nodes = "";
		try {
			// get the filtered nodes
			var nodes = samlDoc.findall("*" + config.casErpidXpath);
			var erpIds = [];
			for (var i = 0; i < nodes.length; i++) {
				//console.log(nodes[i].text);
				var roleGroup = nodes[i].text;
				//console.log(roleGroup);
				if (config.casErpidExtractGroup && roleGroup) {
					//casErpidExtractGroup
					if (roleGroup.toLowerCase().indexOf(config.casErpidExtractGroup.toLowerCase()) >= 0) {
						var regExp = new RegExp('(' + config.casErpidExtractGroup + ')', 'gi');
						roleGroup = roleGroup.replace(regExp, '');
						roleGroup = roleGroup ? roleGroup.trim() : '';
						if (roleGroup && roleGroup.substr(roleGroup.length - 1) == ',') {
							roleGroup = roleGroup.substr(0, roleGroup.length - 1);
						}
						//casErpidExtract
						if (roleGroup && config.casErpidExtract) {
							regExp = new RegExp('(' + config.casErpidExtract + ')', 'gi');
							roleGroup = roleGroup.replace(regExp, '');
							roleGroup = (roleGroup.length > 1 && roleGroup.substr(0, 1) == '=') ? roleGroup.substr(1) :
								roleGroup;
						}
						erpIds.push(roleGroup);
					}
				}
			}
			var erpId = (erpIds.length > 0) ? erpIds[0] : "";
			return callBack(null, tempStore, activeRoles, erpId);
		} catch (ex) {
			logger.error(__filename, ' - Error filtering cas.role.xpath from SAML doc. Error: ' + ex);
			return callBack(null, tempStore, activeRoles, '');
		}
	}
}


module.exports = {
	authenticate: function authenticate(tempStore, callBack) {
		logger.info(__filename, "- Authenticating using CAS: ", tempStore.config.authConfig);
		async.waterfall([
				async.apply(getTicketGrantingTicket, tempStore),
				getServiceTicket,
				getSamlResponse,
				getSAMLDocument,
				getUserRoles,
				getUserErpId
			],
			function(err, tempStore, activeRoles, erpId) {
				if (err) {
					return callBack(err);
				}
				//console.log("appname=" + tempStore.config.appName);
				//console.log(activeRoles);
				erpId = (erpId) ? erpId : "";
				activeRoles = (activeRoles && activeRoles.length > 0) ? activeRoles : [];
				// Create uUserPrincipal Object and return
				var userPrincipal = principal.newUserPrincipal(tempStore.username,
					'', erpId, activeRoles,
					tempStore.config.tenant.name,
					tempStore.config.appName);
				logger.info(__filename, "User ", tempStore.username, " authenticated successfully.",
					userPrincipal);
				tempStore.userPrincipal = userPrincipal;
				callBack(null, tempStore);
			})
	}
}
