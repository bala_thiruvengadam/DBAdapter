var ldap = require('ldapjs'),
  _ = require('lodash'),
  async = require('async'),
  crypto = require('../utils/crypto.server.util'),
  logger = require('../utils/logger.server.util'),
  principal = require('../models/UserPrincipal');

function destroyLdapClient(ldapclient) {
  if (!ldapclient) {
    return
  } else {
    ldapclient.unbind();
    ldapclient.destroy();
    return;
  }
}

function testConnectivity(config, callBack) {
  var ldapConfig = config.authConfig;
  logger.debug(__filename, "LDAP configuration:")
  logger.debug(ldapConfig);
  //try {
  // Use the administrative account to find the user
  var adminClient = ldap.createClient({
    url: ldapConfig.ldapUrl,
    //queueDisable: true,  : commeted out as this causes error creating admin client
    timeout: 5000,
    connectTimeout: 5000
  });
  logger.debug(__filename, "ldap client created");
  // Bind as the administrator (or a read-only user), to get the DN for the user attempting to authenticate
  adminClient.bind(ldapConfig.ldapAdminUserDN, crypto.decrypt(ldapConfig.ldapAdminUserPassword),
    function(err) {
      if (err) {
        logger.error(__filename, "Error binding admin user : ");
        logger.error(err);
        destroyLdapClient(adminClient);
        return callBack(err);
      }
      logger.debug(__filename, "LDAP adminuser bind successful");
      destroyLdapClient(adminClient);
      return callBack();
    });
  //} catch (err) {
  //  console.log("Error connecting to LDAP server : ");
  //  console.log(err);
  //  return callBack(err, null);
  //}
}

// roleObjectClass = objectClass = user / person / organizationalPerson
// roleBase =
// roleNameAttr = eduPersonAffiliation
// roleMemberAttr = distinguishedName = CN=allison.lavelle,OU=STUDENTS,OU=LAVERNE,DC=laverne,DC=local
//  (&(objectClass=user)(distinguishedName=CN=allison.lavelle,OU=STUDENTS,OU=LAVERNE,DC=laverne,DC=local) )

//var username = "hburgos2";
//var password = "S00230760*";
//var username = "Allison.lavelle";
//var password = "Laverne99";

function findUserWithAdminAccount(tempStore, callBack) {
  var config = tempStore.config.authConfig,
    username = tempStore.username,
    password = tempStore.password;

  try {
    // Use the administrative account to find the user
    var adminClient = ldap.createClient({
      url: config.ldapUrl,
      //queueDisable: true,
      timeout: 5000,
      connectTimeout: 5000
    });
    // Bind as the administrator (or a read-only user), to get the DN for the user attempting to authenticate
    adminClient.bind(config.ldapAdminUserDN, crypto.decrypt(config.ldapAdminUserPassword),
      function(err) {
        if (err) {
          logger.error(__filename, "Error binding admin user : " + err);
          return callBack(err, null);
        }
        // Search for a user
        adminClient.search(config.ldapAdminUserBase, {
          scope: "sub",
          filter: "(" + config.ldapUserNameAttr + "=" +
            username + ")"
        }, function(err, ldapResult) {
          if (err) {
            logger.error(__filename,
              "ERROR searching userNameAttr : " +
              err);
            return callBack(err, null);
          }
          ldapResult.on('searchEntry', function(entry) {
            return callBack(null, tempStore,
              entry);
          });
        });
      });
  } catch (err) {
    logger.error(__filename, "Error connecting to LDAP server : ", err);
    return callBack(err);
  }
}

function authenticateUser(tempStore, entry, callBack) {
  var username = tempStore.username,
    password = tempStore.password,
    config = tempStore.config.authConfig;
  try {
    var dn = entry.dn;
    logger.debug(__filename, "- DN=" + dn);
    // When you have the DN, try to bind with it to check the password
    var userClient = ldap.createClient({
      url: config.ldapUrl
    });
    userClient.bind(dn, password, function(err, user) {
      if (err) {
        logger.warn(__filename, "- Authentication failed: " + err);
        return callBack(err, null);
      }
      logger.debug(__filename, "- Successfully authenticated !");
      return callBack(null, tempStore, entry);
    });
  } catch (err) {
    logger.error(__filename, "- Error while authenticating: ", err);
    return callBack(err, null);
  }

}

function getErpId(tempStore, entry, callBack) {
  var config = tempStore.config.authConfig,
    username = tempStore.username,
    password = tempStore.password;
  try {
    // Find the userNameAttr object
    var userNameAttrObj = _.find(entry.attributes, function(attrib) {
      return attrib.type.toUpperCase() == config.ldapUserNameAttr
        .toUpperCase();
    });
    var userNameAttrVal = userNameAttrObj._vals.toString();
    // Find the erpIdAttr
    var erpIdAttrObj, erpIdAttrVal;
    if (config.ldapErpidAttr) {
      erpIdAttrObj = _.find(entry.attributes, function(attrib) {
        return attrib.type.toUpperCase() == config.ldapErpidAttr
          .toUpperCase();
      });
      erpIdAttrVal = erpIdAttrObj._vals.toString();
    }
    logger.debug(__filename, "userNameAttr val=" + userNameAttrVal + " , erpIdAttr val=" +
      erpIdAttrVal);
    return callBack(null, tempStore, entry,
      userNameAttrVal, erpIdAttrVal);
  } catch (ex) {
    logger.error(__filename, "- Error whie getting ERPID :", err);
    return callBack(err, null);
  }
}

function getUserRoles(tempStore, entry, userNameAttr, erpId,
  callBack) {
  var config = tempStore.config.authConfig;
  try {
    var roles = [],
      roleObj;
    if (config.ldapRoleNameAttr) {
      roleObj = _.find(entry.attributes, function(attrib) {
        return attrib.type.toUpperCase() == config.ldapRoleNameAttr
          .toUpperCase();
      });
      if (roleObj) {
        roles.push(roleObj);
      }
    }
    return callBack(null, tempStore, userNameAttr, erpId,
      roles);
  } catch (ex) {
    return callBack(err, null);
  }
}


module.exports = {
  authenticate: function authenticate(tempStore, callBack) {
    async.waterfall([
        async.apply(findUserWithAdminAccount, tempStore),
        authenticateUser,
        getErpId,
        getUserRoles
      ],
      function(err, tempStore, userNameAttr,
        erpId, roles) {
        if (err) {
          return callBack(err);
        }
        // Create uUserPrincipal Object and return
        var userPrincipal = principal.newUserPrincipal(
          tempStore.username,
          '', erpId, roles,
          tempStore.config.tenant.name,
          tempStore.config.appName);
        tempStore.userPrincipal = userPrincipal;
        return callBack(null, tempStore);
      });
  },
  testLdapConnection: function testLdapConnection(config, callBack) {
    logger.info(__filename, "Testing LDAP connectivity:");
    logger.debug(config.authConfig);
    testConnectivity(config, function(err) {
      var resObj;
      if (err) {
        resObj = {
          "error": err.lde_message
        }
      } else {
        resObj = {
          "message": "LDAP connection successful"
        }
      }
      logger.info(resObj);
      return callBack(null, resObj);
    });
  }
}
