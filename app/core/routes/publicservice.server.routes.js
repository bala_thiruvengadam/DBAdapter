'use strict';

var async = require('async'),
  _ = require('lodash'),
  cacheService = require('../services/cache.server.services'),
  server = require('../controllers/publicservice.server.controller'),
  logger = require('../utils/logger.server.util');


function convertURLParamsToJSON(params) {
  var sourceParams = params.split('&'),
    bodyParams = {};
  _.each(sourceParams, function(sparam) {
    var keyVal = sparam.split('=');
    bodyParams[keyVal[0]] = (keyVal.length == 2) ? keyVal[1] : "";
  });
  return bodyParams;
}

function getTenantConfigs(tempStore, callBack) {
  cacheService.getCachedObject(tempStore.tenantApp + '.config', function(err, config) {
    if (err) {
      err.statuscode = 500;
      err.error = "Internal server error";
      return callBack(err);
    }
    if (!config || config == undefined) {
      // config not found in cache. Get config from database and add to cache
      tempStore.config = config;
      return callBack(null, tempStore);
    }
    tempStore.config = config;
    return callBack(null, tempStore);
  });
}



module.exports = function(app) {
  app.use('/:tenantapp/services/public', function(req, res, next) {
    var tenantApp = req.params.tenantapp,
      tempStore = {};
    tempStore.tenantApp = tenantApp;
    ////////////////////////////////////////////////////////////////////////////
    var contentType = req.headers['content-type'];
    if (contentType && contentType.indexOf('application/json') == 0) {
      try {
        var params = convertURLParamsToJSON(req.body.body);
        _.forIn(params, function(value, key) {
          req.body[key] = value;
        });
      } catch (err) {
        //just bypass
      }
    }
    ////////////////////////////////////////////////////////////////////////////
    async.waterfall([
      async.apply(getTenantConfigs, tempStore)
    ], function(err, tempStore) {
      if (err) {
        res.writeHead(err.statuscode, {
          'Content-Type': 'application/json'
        });
        res.end(err.error);
        return;
      }
      req.params.tenant = tempStore.config.tenant.name;
      req.tenantConfig = tempStore.config;
      next();
    });
  });


  app.get('/:tenantapp/services/public/:module', server.processGet);
  app.get('/:tenantapp/services/public/:module/:inputParams', server.processGetWithParams);
  app.post('/:tenantapp/services/public/:module', server.processPost);

}
