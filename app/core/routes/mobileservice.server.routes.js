'use strict';

var async = require('async'),
	logger = require('../utils/logger.server.util'),
	cacheService = require('../services/cache.server.services'),
	server = require('../controllers/mobileservice.server.controller'),
	utils = require('../utils/mobile.server.util'),
	productConfig = require('../../ui/models/ProductConfig.server.model');


function findTenantConfigFromDB(tempStore, callBack) {
	var appName = tempStore.tenantApp;
	productConfig.findByTenantApp(appName, function(err, tenantConfig) {
		if (err) {
			return callBack(err);
		}
		if (tenant) {
			tempStore.config = tenantConfig;
			return callBack(null, tempStore);
		} else {
			return callBack({
				"error": "Tenant Not found"
			});
		}
	});
}

function addConfigToCache(tempStore, callBack) {
	var appName = tempStore.tenantApp;
	var config = tempStore.config;
	cacheService.addToCache(appName + '.config',
		JSON.stringify(config),
		'',
		function(err, success) {
			if (err) {
				return callBack(err);
			}
			logger.info("cached config for " + appName);
			return cb(null, tempStore);
		});
}

function getTenantConfigs(tempStore, callBack) {
	logger.info(__filename, "- Getting tenant configuration:");
	cacheService.getCachedObject(tempStore.tenantApp + '.config', function(err, config) {
		if (err || !config || config == undefined) {
			// Tenant Config not found in cache. Check if config exists in DB or not.
			async.waterfall([
				async.apply(findTenantConfigFromDB, tempStore),
				addConfigToCache
			], function(err, tempStore) {
				if (err) {
					err.statuscode = 500;
					err.error = "Internal server error";
					return callBack(err);
				}
				return callBack(null, tempStore);
			});
		}
		tempStore.config = config;
		return callBack(null, tempStore);
	});
}

function getUserPrincipal(tempStore, callBack) {
	if (tempStore.ticket) {
		var cacheTicket = tempStore.tenantApp + ".ticket." + tempStore.ticket;
		cacheService.getCachedObject(cacheTicket, function(err, principal) {
			if (err || !principal || !principal.username) {
				err = {}
				err.statuscode = 403;
				err.error = "Forbidden";
				return callBack(err);
			}
			tempStore.userPrincipal = principal;
			return callBack(null, tempStore);
		});
	}
}

module.exports = function(app) {
	app.use('/:tenantapp/services/student', function(req, res, next) {
		var ticket = req.query.ticket,
			tenantApp = req.params.tenantapp,
			tempStore = {};
		tempStore.ticket = ticket;
		tempStore.tenantApp = tenantApp;
		////////////////////////////////////////////////////////////////////////////
		async.waterfall([
			async.apply(getTenantConfigs, tempStore),
			getUserPrincipal
		], function(err, tempStore) {
			if (err) {
				res.status(403).send('');
				return;
			}
			if (tempStore.userPrincipal) {
				req.params.tenant = tempStore.config.tenant.name;
				req.userPrincipal = tempStore.userPrincipal;
				req.tenantConfig = tempStore.config;
				logger.debug("Tenant= " + req.params.tenant);
				next()
			} else {
				res.status(403).send('');
				return;
			}
		});
	});

	app.get('/listcache', function(req, res) {
		cacheService.listCache(function(err, results) {
			res.writeHead(200, {
				'Content-Type': 'application/json'
			});
			res.end(JSON.stringify(results));
		});
	})


	app.get('/:tenantapp/services/student/:module', server.processGet);
	app.get('/:tenantapp/services/student/:module/:inputParams', server.processGetWithParams);
	app.post('/:tenantapp/services/student/:module', server.processPost);

}
