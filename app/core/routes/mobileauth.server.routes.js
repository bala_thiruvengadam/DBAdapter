'use strict';
var bodyParser = require('body-parser'),
	_ = require('lodash');
////////////////////////////////////////////////////////////////////////////////
//	Routes for handling mobile authentication requests:
//	Intercepts the mobile authentication requests and calls the
//	controller "mobileauth.server.controller" for authentication.
////////////////////////////////////////////////////////////////////////////////
module.exports = function(app) {
	var server = require('../controllers/mobileauth.server.controller.js');


	app.post('/:tenantapp/services/authenticate/login', server.authenticate);

}
