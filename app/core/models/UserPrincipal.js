///////////////////////////////////////////////////////////////////////////
///   UserPrincipal model
///////////////////////////////////////////////////////////////////////////
var UserPrincipal = function(username, pidm, erpid, roles, tenant,
	tenantApp) {
	this.username = username;
	this.pidm = pidm;
	this.erpid = erpid;
	this.roles = (roles) ? roles : [];
	this.tenant = tenant;
	this.tenantApp = tenantApp;
	this.lastUpdated = new Date();
}

module.exports = {
	newUserPrincipal: function(username, pidm, erpid, roles, tenant, tenantApp) {
		return new UserPrincipal(username, pidm, erpid, roles, tenant, tenantApp);
	}
}
