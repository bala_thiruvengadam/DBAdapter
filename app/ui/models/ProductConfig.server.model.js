'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var crypto = require('../../core/utils/crypto.server.util'),
  cacheService = require('../../core/services/cache.server.services');

var ProductConfigSchema = new Schema({
  tenant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Tenant'
  },
  appName: {
    type: String,
    required: true,
    unique: true,
    trim: true
  },
  configName: {
    type: String,
    required: true,
    trim: true
  },
  active: {
    type: Boolean
  },
  licenseKey: {
    type: String
  }, // validate: customValidator
  licenseValid: {
    type: Boolean
  },
  cacheSource: {
    type: String,
    enum: ["MEM", "DB"],
    default: "MEM"
  },
  authType: {
    type: String,
    enum: ["CAS", "LDAP", "SSB"]
  },
  lastUpdated: {
    type: Date,
    default: Date.now
  },
  dataSourceConfig: {
    connectString: {
      type: String
    },
    driver: {
      type: String
    },
    username: {
      type: String
    },
    password: {
      type: String
    },
    poolMax: {
      type: Number,
      default: 5
    },
    poolMin: {
      type: Number,
      default: 2
    },
    poolIncrement: {
      type: Number,
      default: 1
    },
    poolTimeout: {
      type: Number,
      default: 60
    },
    poolPingInterval: {
      type: Number,
      default: 60
    },
    queueRequests: {
      type: Boolean,
      default: true
    },
    queueTimeout: {
      type: Number,
      default: 5000
    },
    maxRows: {
      type: Number,
      default: 100
    },
    testSQL: {
      type: String,
      default: "Select 1 from dual"
    }
  },
  authConfig: {
    casRootContext: {
      type: String
    },
    casAppServerHost: {
      type: String
    },
    casAppContext: {
      type: String
    },
    casRoleXpath: {
      type: String
    },
    casRoleExtractGroup: {
      type: String
    },
    casRoleExtractRole: {
      type: String
    },
    casRoleRegex: {
      type: String
    },
    casErpidXpath: {
      type: String
    },
    casErpidExtractGroup: {
      type: String
    },
    casErpidExtract: {
      type: String
    },
    casErpidRegex: {
      type: String
    },
    ldapUrl: {
      type: String
    },
    ldapRootBase: {
      type: String
    },
    ldapAdminUserDN: {
      type: String
    },
    ldapAdminUserPassword: {
      type: String
    },
    ldapAdminUserBase: {
      type: String
    },
    ldapUserNameAttr: {
      type: String
    },
    ldapUserBase: {
      type: String
    },
    ldapErpidAttr: {
      type: String
    },
    ldapRoleObjectclass: {
      type: String
    },
    ldapRoleBase: {
      type: String
    },
    ldapRoleNameAttr: {
      type: String
    },
    ldapMemberAttr: {
      type: String
    },
    ssbLoginScript: {
      type: String
    }
  },
  additional: {}
});

ProductConfigSchema.pre('save', function(next) {
  //console.log("in pre save schema");
  var config = this;

  //console.log(config.isModified('dataSourceConfig.password'));
  if (config.dataSourceConfig.password && config.isModified('dataSourceConfig.password')) {
    config.dataSourceConfig.password = crypto.encrypt(config.dataSourceConfig.password);
  }
  if (config.authConfig.ldapAdminUserPassword && config.isModified(
      'authConfig.ldapAdminUserPassword')) {
    config.authConfig.ldapAdminUserPassword = crypto.encrypt(config.authConfig.ldapAdminUserPassword);
  }
  next();
});

ProductConfigSchema.post('save', function(next) {
  //console.log(this.toJSON());
  // Add/replace the config to cache
  var tenantApp = this.appName;
  cacheService.addToCache(tenantApp + '.config',
    JSON.parse(JSON.stringify(this)),
    '',
    function(err, success) {
      if (err) {
        console.log("Error while caching tenant config for " + tenantApp);
      } else {
        console.log("Tenant config for " + tenantApp + " added to cache.");
      }
    });
});


ProductConfigSchema.statics.findByTenantApp = function(appname, callBack) {
  this.findOne({
      appName: appname
    })
    .populate('tenant')
    .exec(function(err, result) {
      if (err) {
        return callBack(err);
      }
      return callBack(null, result);
    });
}

module.exports = mongoose.model("ProductConfig", ProductConfigSchema,
  "ProductConfig");
