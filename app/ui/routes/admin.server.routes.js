'use strict';

var express = require('express');
var request = require('request');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mongoose = require('mongoose');
var restify = require('express-restify-mongoose');
var passport = require('passport');
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');
var app = express();
var router = express.Router();
var tenantSchema = require('../models/Tenant.server.model');
var productConfigSchema = require('../models/ProductConfig.server.model');
var adminController = require('../controllers/admin.server.controller');
var secretKey = global.secretKey;

function serialize(req, res, next) {
  //console.log("in serialize...");
  //console.log(JSON.stringify(req.user));
  next();
}

function generateWebToken(req, res, next) {
  req.token = jwt.sign(req.user,
    secretKey, {
      expiresIn: 60 * 10
    });
  next();
}

function showResponse(req, res) {
  res.status(200).json({
    user: {
      firstname: req.user.firstname,
      lastname: req.user.lastname,
      roles: req.user.roles,
      kryptostoken: req.user.token
    },
    token: req.token
  });
}

function verifyAuthToken(req, res, next) {
  // check header or url parameters or post parameters for token
  console.log(JSON.stringify(req.headers));
  var token = req.body.token || req.query.token || req.headers['Authorization'] || req.headers[
    'authorization'];
  //console.log("token=" + token);
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, secretKey,
      function(err, decoded) {
        if (err) {
          if (err.name == 'TokenExpiredError') {
            return res.status(403).send({
              success: false,
              message: 'Token expired'
            });
          } else {
            return res.json({
              success: false,
              message: 'Failed to authenticate token'
            });
          }

        } else {
          console.log("decoded=" + JSON.stringify(decoded));
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          next();
        }
      });
  } else {
    return res.status(403).send({
      success: false,
      message: 'Not authenticated'
    });
  }
};

module.exports = function(app) {
  //app.use(bodyParser.json());
  //app.use(methodOverride());

  console.log("in admin routes");

  app.post('/api/authenticate',
    passport.authenticate('local', {
      session: false
    }), serialize, generateWebToken, showResponse);

  app.get('/logout',
    function(req, res) {
      req.logout();
      res.redirect('/');
    });

  app.post('/licenseValid', adminController.validateLicense);

  app.get('/:appname/testdbconnection', verifyAuthToken, adminController.testDbConnection);

  app.get('/:appname/testauthconnection', verifyAuthToken, adminController.testAuthConnection);

  app.get('/:appname/cleartenantcache', verifyAuthToken, adminController.clearTenantCache);

  app.get('/:appname/cleartenantservicecache', verifyAuthToken, adminController.clearTenantServiceCache);

  app.get('/:appname/cleartenantticketcache', verifyAuthToken, adminController.clearTenantTicketCache);


  app.use('/api/v1', verifyAuthToken);

  restify.serve(app, tenantSchema, {
    name: "tenants",
    runValidators: true
  });

  restify.serve(app, productConfigSchema, {
    name: "products",
    runValidators: true,
    findOneAndUpdate: false
  });

  app.use(router);
}
