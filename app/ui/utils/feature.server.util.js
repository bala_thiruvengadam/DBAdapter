"use strict";

var request = require('request'),
  _ = require('lodash'),
  async = require('async'),
  cache = require('../../core/services/cache.server.services'),
  featureSchema = require('../models/Feature.server.model');


var kryptosServerAPIUrl = global.kryptosURL;



///////////////////////////////////////////////////////////////////////////////
function populateFeaturesFromStore(config, callBack) {
  // Mostly we may not store/retrieve any data from mongo store:
  /*
    featureSchema.findFeaturesByTenantApp(config.appName, function(err, results) {
        //console.log("mong features:");
        //console.log(results);
        if (err) {
            console.log("Error returning features from Mongo Store:");
            console.log(err);
            return callBack();
        }
        return callBack(null, results);
    });
    */
  //Returning empty results as we may not be using this feature any more.
  return callBack();
}

function populateFeaturesFromKryptos(config, callBack) {
  var tenantLicenseKey = config.licenseKey,
    tenant = config.tenant.name;
  var reqUrl = kryptosServerAPIUrl + 'gateway/' + tenant;
  console.log("url=" + reqUrl);
  console.log("Tenant =" + tenant + " - license=" + tenantLicenseKey);
  try {
    request({
      url: reqUrl,
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'licenseKey': tenantLicenseKey
      }
    }, function(err, response, body) {
      if (err) {
        err.statuscode = 500;
        err.error =
          "Communication error with cloud configuration server.";
        return callBack(err);
      }
      if (response.statusCode == 200) {
        if (response.licenseresponse && !response.licenseresponse
          .validity) {
          var err = {
            statuscode: 200,
            error: "Invalid License key."
          };
          return callBack(err);
        } else {
          body = JSON.parse(body);
          console.log("features retrieved, length=" + body.features.length + " - " + body.features[
            0]);
          return callBack(null, body.features);
        }
      } else {
        var err = {
          statuscode: 500,
          error: "Communication error with cloud configuration server."
        };
        return callBack(err, null);
      }
    });
  } catch (err) {
    err.statuscode = 500;
    err.error = "Communication error with cloud configuration server.";
    return callBack(err);
  }
}

function retrieveFeatures(config, callBack) {
  async.parallel({
    "featuesFromKryptos": async.apply(populateFeaturesFromKryptos,
      config),
    "featuresFromStore": async.apply(populateFeaturesFromStore,
      config)
  }, function(err, results) {
    if (err) {
      console.log("Error while getting features from server:");
      console.log(err);
      return callBack(err);
    }
    //console.log("store features:");
    //console.log(JSON.stringify(results.featuresFromStore));
    return callBack(null, config, results.featuesFromKryptos,
      results.featuresFromStore);
  });
}
///////////////////////////////////////////////////////////////////////////////

function clearServiceCache(config, callBack) {
  var appName = config.appName;
  var cacheKey = appName + ".servicecache.";
  cache.removeFromCacheForKeysStartingwith(cacheKey, function() {
    return callBack();
  });
}

function getServicesForCaching(config, featuresFromKryptos, featuresFromStore,
  callBack) {
  var appName = config.appName;
  var cacheKey = appName + ".servicecache.",
    allowedServices = [],
    mServices = [],
    isEnabled = true,
    mRoles = [],
    mServiceItem = {};
  console.log("Total features for app " + appName + " is " + featuresFromKryptos.length);
  _.forEach(featuresFromKryptos, function(kFeature) {
    var mFeature = _.find(featuresFromStore, function(o) {
      return o.featurename == kFeature.featurename;
    });
    mServices = [];
    if (!mFeature == undefined) {
      mServices = mFeature.services;
    }
    //console.log("Features from Kryptos. service "+ kFeature.featurename + " - " );
    _.forEach(kFeature.services, function(kService) {
      var mService = _.find(mServices,
        function(o) {
          return o == kService.servicename;
        });
      isEnabled = (mService == undefined) ?
        true : mService.enabled;
      mRoles = (mService == undefined) ? [] : mService.roles;
      //Add service to collection: service cache data in the following format
      // { service : "kmwda1mwcc.servicecache.bannergetuserroles", enabled: true, roles: [] }
      allowedServices.push({
        service: cacheKey + kService.servicename,
        enabled: isEnabled,
        roles: mRoles
      });
    });
  });
  console.log("Allowed services count " + allowedServices.length);
  return callBack(null, config, allowedServices);
}

function addToCache(config, service, callBack) {
  //console.log("caching service "+ service.service);
  cache.addToCache(service.service, service, config, function(err, success) {
    if (err) {
      console.log("Error while caching service: " + service.service);
      console.log(err);
    }
    return callBack();
  });
}

function addServicesToCache(config, allowedServices, callBack) {
  async.map(allowedServices,
    async.apply(addToCache, config),
    function(err, results) {
      //console.log("Features cached successfully");
      return callBack(null, true);
    });
}

function setServicesForCaching(config, featuresFromKryptos, featuresFromStore,
  callBack) {
  async.parallel({
    result1: async.apply(clearServiceCache, config),
    result2: async.apply(getServicesForCaching, config,
      featuresFromKryptos,
      featuresFromStore)
  }, function(err, results) {
    if (!results || !results.result2[1] || results.result2[1].length == 0) {
      console.log("ERROR: No active services found");
      return callBack();
    }
    //console.log("printing results of allowed services:");
    //console.log(results.result2);
    //return config, allowedServices
    return callBack(null, results.result2[0], results.result2[1]);
  });

}

////////////////////////////////////////////////////////////////////////////////
exports.loadFeatureServices = function(config, callBack) {
  console.log("in loadFeatureServices");
  async.waterfall([
    async.apply(retrieveFeatures, config),
    setServicesForCaching,
    addServicesToCache
  ], function(err, success) {
    if (err) {
      console.log("Error occured while loading features:");
      console.log(err);
      return callBack(err);
    }
    return callBack(null, success);
  });
}

exports.isServiceEnabled = function(config, module, callBack) {
  var cacheKey = config.appName + ".servicecache." + module;
  cache.getCachedObject(cacheKey, function(err, value) {
    if (err) {
      return callBack(null, false);
    }
    return callBack(null, value.enabled);
  });
}

exports.isServiceAuthorized = function(config, module, roles, callBack) {
  var cacheKey = config.appName + ".servicecache." + module;
  cache.getCachedObject(cacheKey, function(err, value) {
    if (err) {
      return callBack(null, false);
    }
    var mRoles = value.roles,
      authorized = true;
    if (!mRoles || mRoles.length <= 0) {
      authorized = true;
    } else {
      if (roles && roles.length > 0) {
        _.forEach(roles, function(role) {
          var findRole = _.find(mRoles, function(
            o) {
            return o == role
          });
          if (findRole) {
            authorized = true;
            return;
          }
        });
      }
    }
    return callBack(null, authorized);
  });
}

exports.getCachedService = function getCachedService(tenantApp, module,
  callBack) {
  var cacheKey = tenantApp + ".servicecache." + module;
  cache.getCachedObject(cacheKey, function(err, value) {
    if (err) {
      return callBack();
    }
    return callBack(null, value);
  });
}
