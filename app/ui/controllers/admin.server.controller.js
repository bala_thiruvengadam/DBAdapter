var request = require('request'),
  async = require('async'),
  oracledb = require('oracledb'),
  cacheService = require('../../core/services/cache.server.services'),
  baseservice = require('../../core/services/mobilebase.server.services'),
  ldapService = require('../../core/services/ldapauth.server.services');



function getTenantConfigs(tenantApp, callBack) {
  cacheService.getCachedObject(tenantApp + '.config', function(err, config) {
    if (err) {
      err.statuscode = 500;
      err.error = "Internal server error";
      return callBack(err);
    }
    if (!config || config == undefined) {
      err.statuscode = 500;
      err.error = "Internal server error";
      return callBack(err);
    }
    console.log("tenant config found on cache:");
    console.log(JSON.stringify(config));
    return callBack(null, config);
  });
}

function testDBConnection(config, callBack) {
  baseservice.testDBConnection(config, function(err, result) {
    if (err) {
      console.log("in testdbconnection: error");
      console.log(err);
      return callBack(err);
    }
    return callBack(null, result);
  });
}

function verifyCasConnection(config, callBack) {
  var casRoot = config.authConfig.casRootContext;
  var resObj = {};
  if (casRoot) {
    console.log("cas url: " + casRoot);
    request({
      url: casRoot,
      method: 'GET',
      headers: {
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36'
      }
    }, function(err, response, body) {
      console.log("Error:" + err);
      console.log(response.statusCode);
      if (err) {
        resObj = {
          "error": "ERROR: CAS connection"
        }
        return callBack(null, resObj);
      } else {
        if (response.statusCode == 200 || response.statusCode == 302) {
          resObj = {
            "message": "CAS server connection successful."
          }
        } else {
          resObj = {
            "error": "CAS server connection failed."
          }
        }
        return callBack(null, resObj);
      }
    });
  } else {
    resObj = {
      "error": "CAS server connection failed."
    }
    return callBack(null, resObj);
  }
}

function verifyLdapConnection(config, callBack) {
  ldapService.testLdapConnection(config, function(err, message) {
    return callBack(null, message);
  });
}

function testAUTHConnection(config, callBack) {
  switch (config.authType) {
    case "CAS":
      //CAS auth
      verifyCasConnection(config, function(err, result) {
        if (err) {
          return callBack(err);
        }
        return callBack(null, result);
      });
      break;
    case "LDAP":
      //LDAP auth
      verifyLdapConnection(config, function(err, result) {
        if (err) {
          return callBack(err);
        }
        return callBack(null, result);
      });
      break;
    case "SSB":
      // SSB Auth
      //
      break;
    default:
      //Error:
      break;
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////

exports.validateLicense = function(req, res) {
  var tenant = req.body.tenant,
    licenseKey = req.body.licenseKey,
    url = global.kryptosURL;
  if (tenant) {
    request({
      url: url + "validatelicense/" + tenant,
      method: 'GET',
      headers: {
        'licenseKey': licenseKey
      }
    }, function(err, response, body) {
      if (err) {
        console.log("Error: " + err);
        res.writeHead(200, {
          'Content-Type': 'application/json'
        });
        res.end({
          "licenseresponse": {
            "validity": "false",
            "message": "Invalid License key."
          }
        });
      } else {
        res.writeHead(200, {
          'Content-Type': 'application/json'
        });
        res.end(body);
      }
    });
  } else {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end({
      "licenseresponse": {
        "validity": "false",
        "message": "Invalid License key."
      }
    });
  }
};



exports.testDbConnection = function testDbConnection(req, res) {
  var tenantApp = req.params.appname;
  async.waterfall([
    async.apply(getTenantConfigs, tenantApp),
    testDBConnection
  ], function(err, result) {
    console.log("results:");
    console.log(err);
    var outRes;
    if (err) {
      outRes = JSON.stringify({
        "error": err.toString()
      });
    } else {
      outRes = JSON.stringify({
        "message": "Connection successful ! "
      });
    }
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(outRes);
  });
}

exports.testAuthConnection = function(req, res) {
  var tenantApp = req.params.appname;
  async.waterfall([
    async.apply(getTenantConfigs, tenantApp),
    testAUTHConnection
  ], function(err, result) {
    console.log("results:");
    console.log(err);
    var outRes;
    if (err) {
      outRes = JSON.stringify({
        "error": "Error testing authentication connectivity."
      });
    } else {
      outRes = JSON.stringify(result);
    }
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(outRes);
  });
}

exports.clearTenantServiceCache = function(req, res) {
  var tenantApp = req.params.appname;
  cacheService.removeFromCacheForKeysStartingwith(tenantApp + ".module.", function() {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(JSON.stringify({
      "message": "Cleared the tenant services cache."
    }));
  });
}

exports.clearTenantTicketCache = function(req, res) {
  var tenantApp = req.params.appname;
  cacheService.removeFromCacheForKeysStartingwith(tenantApp + ".ticket.", function() {
    res.writeHead(200, {
      'Content-Type': 'application/json'
    });
    res.end(JSON.stringify({
      "message": "Cleared the tenant services cache."
    }));
  });
}


exports.clearTenantCache = function(req, res) {
  var tenantApp = req.params.appname;
  cacheService.removeFromCacheForKeysStartingwith(tenantApp + ".module.", function() {
    cacheService.removeFromCacheForKeysStartingwith(tenantApp + ".ticket.", function() {
      res.writeHead(200, {
        'Content-Type': 'application/json'
      });
      res.end(JSON.stringify({
        "message": "Cleared the tenant services cache."
      }));
    });
  });
}
