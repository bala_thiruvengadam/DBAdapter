angular.module('appModule')
  .controller('LoginController', function($scope, $rootScope, $http, $location, $window, $route,
    $routeParams) {
    console.log("in Login controller");


    $scope.newTenant = {};
    $scope.errorMessage = "";



    $scope.authUser = function() {
      console.log("in login submit");
      var data = {
        "username": $scope.username,
        "password": $scope.password
      };

      $http.post('/api/authenticate', data).then(function(response) {
        console.log("response:");
        console.log(response);
        if (response.data.token) {
          $rootScope.token = response.data.token;
          $rootScope.user = response.data;
          $window.localStorage.setItem("token", response.data.token);
          $window.localStorage.setItem("user", response.data);
          $location.path('/home');
        }

      });
    }

  });
