angular.module('appModule')
  .controller('TenantListController', function($scope, $rootScope, $http, $location, $route,
    $routeParams) {
    console.log("in tenant controller");
    console.log("token=" + $rootScope.token);

    $scope.newTenant = {};
    $scope.errorMessage = "";

    $scope.listTenants = function() {
      console.log("in list tenant");
      $rootScope.getAPI('/api/v1/tenants', function(err, response) {
        if (err) {
          if (err.status == 403) {
            console.log("going to expire session");
            $location.path('/');
          }
          return;
        }
        $scope.tenants = response.data;
      });
      /*
      $http.get('/api/v1/tenants').then(function(response) {
        console.log("response:");
        console.log(response);
        $scope.tenants = response.data;
      });
      */
    }

    $scope.listTenants();
  });
