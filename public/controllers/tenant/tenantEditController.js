angular.module('appModule')
  .controller('TenantEditController', function($scope, $rootScope, $http, $location, $route,
    $routeParams) {
    console.log("in tenant edit controller");
    console.log("token=" + $rootScope.token);
    var id = $routeParams.tenantid;
    console.log($routeParams);

    $scope.errorMessage = "";

    $rootScope.getAPI('/api/v1/tenants/' + id, function(err, response) {
      if (err) {
        if (err.status == 403) {
          $location.path('/');
        }
        return;
      }
      $scope.newTenant = response.data;
      console.log("Tenant:");
      console.log($scope.newTenant);
      $rootScope.getAPI('/api/v1/products?query={"tenant":"' + id + '"}', function(err,
        response) {
        if (err) {
          console.log(err);
          $scope.errorMessage = "Tenant not found !";
          return;
        }
        $scope.products = response.data;
        console.log("products:");
        console.log($scope.products);
      });
    });


    $scope.updateTenant = function() {
      $rootScope.putAPI('/api/v1/tenants/' + $scope.newTenant._id, $scope.newTenant, function(
        err, response) {
        if (err) {
          console.log(err);
          return;
        }
        console.log(response);
        $location.path('/admin/tenant/list');
      });
    }

    $scope.cancelAction = function() {
      $location.path('/admin/tenant/list');
    }

  });
