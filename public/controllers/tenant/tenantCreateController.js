angular.module('appModule')
  .controller('TenantCreateController', function($scope, $rootScope, $http, $location, $route,
    $routeParams) {
    console.log("in tenant create controller");
    $scope.newTenant = {};
    $scope.errorMessage = "";

    $scope.createTenant = function() {
      console.log($scope.newTenant);
      $rootScope.postAPI('/api/v1/tenants', $scope.newTenant, function(
        err, response) {
        if (err) {
          console.log(err);
          $scope.errorMessage = error.data.message;
          return;
        }
        console.log(response);
        $location.path('/admin/tenant/list');
      });
    }

    $scope.cancelAction = function() {
      $location.path('/admin/tenant/list');
    }

  });
