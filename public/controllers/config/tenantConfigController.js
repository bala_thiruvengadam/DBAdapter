angular.module('appModule')
  .controller('TenantConfigController', function($scope, $rootScope, $http, $location, $route,
    $routeParams) {
    console.log("in tenant config controller");

    var tenantId = $routeParams.tenantid;
    var id = $routeParams.configid;
    console.log(id + " - " + tenantId);

    $scope.addMode = false;
    $scope.authTypes = ["CAS", "LDAP", "SSB"];
    $scope.authMessage = "";
    $scope.dbMessage = "";
    $scope.errorMessage = "";
    $scope.productConfig = {};
    $scope.backupConfig = {};
    $scope.alertClass = "alert-success";


    if (tenantId) {
      $rootScope.getAPI('/api/v1/tenants/' + tenantId, function(err, response) {
        if (err) {
          if (err.status == 403) {
            console.log("going to expire session");
            $location.path('/');
          }
          return;
        }
        $scope.tenant = response.data;
      });
    }
    if (id) {
      $rootScope.getAPI('/api/v1/products/' + id, function(err, response) {
        if (err) {
          if (err.status == 403) {
            $location.path('/');
            return
          }
          console.log(error);
          $scope.errorMessage =
            "Error while retrieving the production configuration information.";
          return;
        }
        $scope.productConfig = response.data;
        $scope.backupConfig = $scope.productConfig;
        console.log("Product config:");
        console.log($scope.productConfig);
      });
    } else {
      $scope.addMode = true;
      $scope.editMode = true;
    }

    $scope.saveConfig = function(section) {
      //console.log($location);
      //Save configuration
      if ($scope.productConfig._id) {
        $rootScope.putAPI('/api/v1/products/' + $scope.productConfig._id, $scope.productConfig,
          function(err, response) {
            if (err) {
              if (err.status == 403) {
                $location.path('/');
                return
              }
              $scope.errorMessage = err.data.message;
              $scope.alertClass = "alert-danger";
              console.log("Error: " + error);
              return;
            }
            $scope.productConfig = response.data;
            $scope.editMode = false;
            $scope.backupConfig = $scope.productConfig;
            $scope.errorMessage = "Tenant configuration saved successfully !";
            $scope.alertClass = "alert-success";
            console.log($scope.productConfig);
            if (section == 'DB') {
              $scope.testDbConnection();
            } else {
              $scope.testauthconnection();
            }

            //validate license
            $scope.validateLicense($scope.tenant.name,
              $scope.productConfig.licenseKey);
          });
      } else {
        $scope.productConfig.tenant = tenantId;
        $rootScope.postAPI('/api/v1/products', $scope.productConfig, function(err, response) {
          if (err) {
            if (err.status == 403) {
              $location.path('/');
              return
            }
            $scope.errorMessage = err;
            $scope.alertClass = "alert-danger";
            console.log("Error: " + error);
            return;
          }
          $scope.productConfig = response.data;
          $scope.backupConfig = $scope.productConfig;
          $scope.errorMessage = "Tenant configuration saved successfully !";
          $scope.alertClass = "alert-success";
          $scope.addMode = false;
          $scope.editMode = false;
          console.log($scope.productConfig);
          //validate license
          $scope.validateLicense($scope.tenant.name,
            $scope.productConfig.licenseKey);
        });
      }
    }

    $scope.validateLicense = function(tenant, licenseKey) {
      var url = "/licenseValid";
      var postdata = {
        "tenant": tenant,
        "licenseKey": licenseKey
      }
      $http.post(url, postdata)
        .then(function(response) {
          console.log(response);
          $scope.productConfig.licenseValid = (response.data.licenseresponse.validity ==
            "true") ? true : false;
        }, function(error) {
          return false;
        });
    }


    $scope.testDbConnection = function() {
      console.log("testing db connection");
      var tenantApp = $scope.productConfig.appName;
      if (tenantApp) {
        $rootScope.getAPI('/' + tenantApp + "/testdbconnection", function(err, res) {
          if (err) {
            if (err.status == 403) {
              $location.path('/');
              return
            }
            $scope.errorMessage = err;
            $scope.alertClass = "alert-danger"
            return;
          }
          var response = res.data;
          console.log(response);
          if (response && response.error) {
            $scope.errorMessage = response.error;
            $scope.alertClass = "alert-danger"
          } else if (response && response.message) {
            $scope.errorMessage = response.message;
            $scope.alertClass = "alert-success"
          }
        });
      }
    }

    $scope.testauthconnection = function() {
      console.log("Testing auth connection");
      var tenantApp = $scope.productConfig.appName;
      if (tenantApp) {
        $rootScope.getAPI('/' + tenantApp + "/testauthconnection", function(err, res) {
          if (err) {
            if (err.status == 403) {
              $location.path('/');
              return
            }
            $scope.errorMessage = err;
            $scope.alertClass = "alert-danger"
            return;
          }
          var response = res.data;
          console.log(response);
          if (response && response.error) {
            $scope.errorMessage = response.error;
            $scope.alertClass = "alert-danger"
          } else if (response && response.message) {
            $scope.errorMessage = response.message;
            $scope.alertClass = "alert-success"
          }
        });
      }
    }

    $scope.saveDBInfo = function() {
      $scope.saveConfig('DB');
      //test db connectivity
      //$scope.testDbConnection();
    }

    $scope.cancelDB = function() {
      $scope.productConfig = $scope.backupConfig;
      $scope.editMode = false;
    }

    $scope.saveAuth = function() {
      $scope.saveConfig('AUTH');
      //test auth connectivity
      //$scope.testauthconnection();
    }

    $scope.cancelAuth = function() {
      $scope.productConfig = $scope.backupConfig;
      $scope.editMode = false;
    }

    $scope.cancelAction = function() {
      $location.path('/admin/tenant/list');
    }

    $scope.clearTenantCache = function() {
      console.log("clearing tenant ticket cache");
      var tenantApp = $scope.productConfig.appName;
      if (tenantApp) {
        $rootScope.getAPI('/' + tenantApp + "/cleartenantcache", function(err, res) {
          if (err) {
            if (err.status == 403) {
              $location.path('/');
              return
            }
            $scope.errorMessage = err;
            $scope.alertClass = "alert-danger"
            return;
          }
          var response = res.data;
          console.log(response);
          if (response && response.error) {
            $scope.errorMessage = response.error;
            $scope.alertClass = "alert-danger"
          } else if (response && response.message) {
            $scope.errorMessage = response.message;
            $scope.alertClass = "alert-success"
          }
        });
      }
    }

  });
