var  _ =  require('lodash');

var configs = [{
	"appname": "kmwda1MWCC",
	"tenant": "MWCC",
	"authmode": "CAS",
	"cas.root.context": "https://iconnect.mwcc.edu/cas",
	"application.server.host": "http://localhost:3000/",
	"application.context": "kmwda1mwcc",
	"cas.role.xpath": "",
	"cas.role.regex": "",
	"cas.erpid.xpath": "",
	"cas.erpid.regex": ""
},
{
	"appname": "kmwda2ULV",
	"tenant": "ULV",
	"authmode": "LDAP",
	"url": 'ldap://localhost:389',
	"baseDN": 'DC=laverne,DC=local',
	"username": 'CN=svc-campusei-test,OU=SERVICE_ACCOUNTS,DC=laverne,DC=local',
	"password": 'C@mpu$77',
	"rootBase": '',
	"userBase": '',
	"userNameAttr": 'ulvPortalID',
	"erpIdAttr": 'ulvPortalID',
	"roleObjectClass": '',
	"roleBase": '',
	"roleNameAttr": '',
	"roleMemberAttr":''
},
{
	"appname": "kmwda1SSB",
	"tenant": "MWCC",
	"authmode": "SSB"
}
]

module.exports = function(tenantapp) {
	var selectedTenantConfig = {};
	_.forEach(configs, function(config) {
		if (tenantapp == config.appname) {
			selectedTenantConfig = config;
			return;
		}
	});
	return selectedTenantConfig;
}
