'use strict';
////////////////////////////////////////////////////////////////////////////////
//	Defining the application specifics and routes
////////////////////////////////////////////////////////////////////////////////
var express = require('express'),
	_ = require('lodash'),
	getBody = require('raw-body'),
	iconv = require('iconv-lite'),
	bodyParser = require('body-parser'),
	request = require('request'),
	methodOverride = require('method-override'),
	passport = require('passport'),
	Strategy = require('passport-local').Strategy,
	logger = require('../app/core/utils/logger.server.util');


function convertURLParamsToJSON(params) {
	if (!params) {
		return '';
	}
	var sourceParams = params.split('&'),
		bodyParams = {};
	try {
		_.each(sourceParams, function(sparam) {
			var keyVal = sparam.split('=');
			bodyParams[keyVal[0]] = (keyVal.length == 2) ? keyVal[1] : "";
		});
	} catch (err) {
		logger.error("Error in convertURLParamsToJSON: while constructing req.body");
		logger.error(err);
	}
	return bodyParams;
}

module.exports = function() {
	// Configure the local strategy for use by Passport.
	//
	// The local strategy require a `verify` function which receives the credentials
	// (`username` and `password`) submitted by the user.  The function must verify
	// that the password is correct and then invoke `cb` with a user object, which
	// will be set at `req.user` in route handlers after authentication.
	passport.use(new Strategy(
		function(username, password, cb) {
			var kdata = {
				"username": username,
				"password": password
			};
			if (!username) {
				return cb(null, false);
			}
			var koptions = {
				//url : "https://kryptos.kryptosmobile.com/api/authenticate",
				url: global.kryptosURL + "api/authenticate",
				body: JSON.stringify(kdata)
			}
			request.post(koptions, function(err, eresp, ebody) {
				if (!err && eresp.statusCode == 200) {
					var respbody = JSON.parse(ebody);
					return cb(null, respbody);
				} else {
					return cb(null, false);
				}

			});

		}));

	var app = express();

	/*	if (process.env.NODE_ENV == 'development') {
			app.use(logger('dev'));
		} else {
			app.use(compress());
		}*/
	logger.info("Overriding Express JS logger...");
	app.use(require('morgan')({
		"stream": logger.stream
	}));
	////////////////////////////////////////////////////////
	// CORS support
	///////////////////////////////////////////////////////
	app.use(function(req, res, next) {
		res.header('Access-Control-Allow-Origin', '*');
		res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
		res.header('Access-Control-Allow-Headers', 'Content-Type');
		next();
	});

	// Middleware to handle JSON data in POST, PUT requests
	app.use(function(req, res, next) {
		var reqType = req.headers['content-type'];
		if (req.method == 'POST' || req.method == 'PUT') {
			if (reqType && reqType.indexOf('application/json') >= 0) {
				// The body part for POST/PUT requests with JSON content-type would be filtered
				// and parsed in this section.
				logger.log('info', __filename, "- Intercepting", req.method, "request with content-type",
					reqType, "...");
				getBody(req, {}, function(err, rawBuf) {
					if (err) {
						logger.error(__filename, " - Error while intercepting request body.", err);
						return next(err);
					}
					req.text = iconv.decode(rawBuf, 'utf-8');
					// Parse as JSON to req.body
					if (!req.body) {
						req.body = {}
					}
					var params;
					try {
						req.body = JSON.parse(req.text);
						// if url encoded values are attached in json:
						// this is for executing APIs from Kryptos platform
						if (req.body.body) {
							params = convertURLParamsToJSON(req.body.body);
							_.forIn(params, function(value, key) {
								req.body[key] = value;
							});
							logger.info(__filename, " - Request body after parsing:", req.body);
							//console.log("Extracted req.body from URLstring**");
							//console.log(JSON.stringify(req.body));
						}
					} catch (err) {
						// Not a valid JSON text
						logger.error(__filename, " - Error while parsing request body- ", err);
						params = convertURLParamsToJSON(req.text);
						_.forIn(params, function(value, key) {
							req.body[key] = value;
						});
						//console.log("Extracted req.body from URLstring");
						//console.log(JSON.stringify(req.body));
					}
					next();
				});
			} else {
				next();
			}
		} else {
			next();
		}
	});

	//support parsing of application/x-www-form-urlencoded post data
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(methodOverride('X-HTTP-Method-Override'));

	app.use(passport.initialize());
	////////////////////////////////////////////////////////////////////////////
	// Define the application routes
	////////////////////////////////////////////////////////////////////////////
	require('../app/core/routes/mobileauth.server.routes.js')(app);
	require('../app/core/routes/mobileservice.server.routes.js')(app);
	require('../app/core/routes/publicservice.server.routes.js')(app);
	// Admin UI API routes
	require('../app/ui/routes/admin.server.routes.js')(app);
	app.use(express.static(__dirname + "/../public"));


	return app;
}
